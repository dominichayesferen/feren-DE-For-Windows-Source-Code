﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StartScreen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StartScreen))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.AppButton = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Userimagebutton = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.WIFIIcon = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Time = New System.Windows.Forms.Label()
        Me.StepIndicator1 = New DevComponents.DotNetBar.Controls.StepIndicator()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.MonthCalendar1 = New System.Windows.Forms.MonthCalendar()
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ZOSAssistantToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Closable = New System.Windows.Forms.CheckBox()
        Me.DesktopBackImage = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.BatteryLevel = New System.Windows.Forms.ProgressBar()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.TerminalIcon = New System.Windows.Forms.PictureBox()
        Me.LookChangerIcon = New System.Windows.Forms.PictureBox()
        Me.SettingsIcon = New System.Windows.Forms.PictureBox()
        Me.NotesIcon = New System.Windows.Forms.PictureBox()
        Me.GalleryIcon = New System.Windows.Forms.PictureBox()
        Me.CalculatorIcon = New System.Windows.Forms.PictureBox()
        Me.CalendarIcon = New System.Windows.Forms.PictureBox()
        Me.StoreIcon = New System.Windows.Forms.PictureBox()
        Me.RiPlayerIcon = New System.Windows.Forms.PictureBox()
        Me.ExplorerIcon = New System.Windows.Forms.PictureBox()
        Me.KittenFoxIcon = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AppButton, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Userimagebutton, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WIFIIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.DesktopBackImage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.TerminalIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookChangerIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SettingsIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NotesIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GalleryIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CalculatorIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CalendarIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StoreIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RiPlayerIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExplorerIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KittenFoxIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Zuaro_OS_deskshade
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.Panel10)
        Me.Panel1.Controls.Add(Me.AppButton)
        Me.Panel1.Controls.Add(Me.PictureBox9)
        Me.Panel1.Controls.Add(Me.Panel9)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 536)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(852, 43)
        Me.Panel1.TabIndex = 0
        '
        'Panel11
        '
        Me.Panel11.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel11.Controls.Add(Me.Panel12)
        Me.Panel11.Location = New System.Drawing.Point(59, 0)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(453, 43)
        Me.Panel11.TabIndex = 10
        '
        'Panel10
        '
        Me.Panel10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Panel10.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Taskbar
        Me.Panel10.Controls.Add(Me.Label13)
        Me.Panel10.Controls.Add(Me.PictureBox12)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Controls.Add(Me.Label12)
        Me.Panel10.Controls.Add(Me.Label14)
        Me.Panel10.Location = New System.Drawing.Point(546, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(143, 43)
        Me.Panel10.TabIndex = 9
        '
        'Label13
        '
        Me.Label13.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label13.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(3, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 43)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = ""
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox12
        '
        Me.PictureBox12.BackgroundImage = CType(resources.GetObject("PictureBox12.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox12.Location = New System.Drawing.Point(7, 5)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(23, 32)
        Me.PictureBox12.TabIndex = 3
        Me.PictureBox12.TabStop = False
        '
        'Label15
        '
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label15.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(38, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 43)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = ""
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label15.Visible = False
        '
        'Label12
        '
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label12.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(73, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 43)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = ""
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label14.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(108, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 43)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = ""
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AppButton
        '
        Me.AppButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.AppButton.Dock = System.Windows.Forms.DockStyle.Left
        Me.AppButton.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Taskbar_logo_new
        Me.AppButton.Location = New System.Drawing.Point(0, 0)
        Me.AppButton.Name = "AppButton"
        Me.AppButton.Size = New System.Drawing.Size(53, 43)
        Me.AppButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.AppButton.TabIndex = 0
        Me.AppButton.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox9.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(518, 9)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(23, 24)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox9.TabIndex = 6
        Me.PictureBox9.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox9, "Avira is Protecting your PC.")
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.PictureBox8)
        Me.Panel9.Controls.Add(Me.Userimagebutton)
        Me.Panel9.Controls.Add(Me.PictureBox5)
        Me.Panel9.Controls.Add(Me.PictureBox3)
        Me.Panel9.Controls.Add(Me.WIFIIcon)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel9.Location = New System.Drawing.Point(660, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(192, 43)
        Me.Panel9.TabIndex = 8
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox8.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.More
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox8.Location = New System.Drawing.Point(35, 5)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox8.TabIndex = 7
        Me.PictureBox8.TabStop = False
        '
        'Userimagebutton
        '
        Me.Userimagebutton.Dock = System.Windows.Forms.DockStyle.Right
        Me.Userimagebutton.Image = CType(resources.GetObject("Userimagebutton.Image"), System.Drawing.Image)
        Me.Userimagebutton.Location = New System.Drawing.Point(152, 0)
        Me.Userimagebutton.Name = "Userimagebutton"
        Me.Userimagebutton.Size = New System.Drawing.Size(40, 43)
        Me.Userimagebutton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Userimagebutton.TabIndex = 1
        Me.Userimagebutton.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox5.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Audio_Icon_3
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox5.Location = New System.Drawing.Point(105, 9)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(24, 25)
        Me.PictureBox5.TabIndex = 4
        Me.PictureBox5.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox3.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Battery_Icon_1_High
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox3.Location = New System.Drawing.Point(127, 9)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(24, 25)
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'WIFIIcon
        '
        Me.WIFIIcon.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.WIFIIcon.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Wifi_Icon
        Me.WIFIIcon.Location = New System.Drawing.Point(73, 9)
        Me.WIFIIcon.Name = "WIFIIcon"
        Me.WIFIIcon.Size = New System.Drawing.Size(24, 25)
        Me.WIFIIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.WIFIIcon.TabIndex = 3
        Me.WIFIIcon.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White_new
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.Time)
        Me.Panel2.Location = New System.Drawing.Point(348, -1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(157, 46)
        Me.Panel2.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Menu
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.Color.Transparent
        Me.Button1.Location = New System.Drawing.Point(77, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 46)
        Me.Button1.TabIndex = 1
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Time
        '
        Me.Time.BackColor = System.Drawing.Color.Transparent
        Me.Time.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Time.ForeColor = System.Drawing.Color.Black
        Me.Time.Location = New System.Drawing.Point(18, 12)
        Me.Time.Name = "Time"
        Me.Time.Size = New System.Drawing.Size(50, 23)
        Me.Time.TabIndex = 0
        Me.Time.Text = "00:00:00"
        Me.Time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'StepIndicator1
        '
        Me.StepIndicator1.CurrentStep = 0
        Me.StepIndicator1.Location = New System.Drawing.Point(830, 7)
        Me.StepIndicator1.Name = "StepIndicator1"
        Me.StepIndicator1.Size = New System.Drawing.Size(10, 10)
        Me.StepIndicator1.TabIndex = 2
        Me.StepIndicator1.Text = "StartIndicator1"
        Me.StepIndicator1.Visible = False
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(679, 473)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(161, 60)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Zuaro OS Beta 1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Build v.1.0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "zuaroos.weebly.com"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Location = New System.Drawing.Point(638, 289)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(214, 248)
        Me.Panel3.TabIndex = 5
        Me.Panel3.Visible = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoEllipsis = True
        Me.Label1.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(9, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(198, 46)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "User1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 49)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(214, 199)
        Me.Panel4.TabIndex = 0
        '
        'Timer1
        '
        Me.Timer1.Interval = 50
        '
        'Panel5
        '
        Me.Panel5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Controls.Add(Me.MonthCalendar1)
        Me.Panel5.Location = New System.Drawing.Point(362, 42)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(223, 163)
        Me.Panel5.TabIndex = 6
        Me.Panel5.Visible = False
        '
        'MonthCalendar1
        '
        Me.MonthCalendar1.Location = New System.Drawing.Point(0, 0)
        Me.MonthCalendar1.Name = "MonthCalendar1"
        Me.MonthCalendar1.TabIndex = 0
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.YellowGreen)
        '
        'Timer2
        '
        Me.Timer2.Interval = 3000
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BackColor = System.Drawing.SystemColors.Highlight
        Me.Panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel6.Controls.Add(Me.Label4)
        Me.Panel6.Controls.Add(Me.Label3)
        Me.Panel6.Controls.Add(Me.PictureBox2)
        Me.Panel6.ForeColor = System.Drawing.Color.White
        Me.Panel6.Location = New System.Drawing.Point(591, 11)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(254, 91)
        Me.Panel6.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Webdings", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label4.Location = New System.Drawing.Point(230, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(21, 19)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "r"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(84, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(152, 34)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Wi-fi is connected," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "connection successful."
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Wifi_Icon
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox2.Location = New System.Drawing.Point(16, 11)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(64, 65)
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.BackColor = System.Drawing.SystemColors.Highlight
        Me.Panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Controls.Add(Me.Label5)
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.PictureBox4)
        Me.Panel7.ForeColor = System.Drawing.Color.White
        Me.Panel7.Location = New System.Drawing.Point(589, 108)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(256, 91)
        Me.Panel7.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(84, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 20)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "User1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Webdings", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label5.Location = New System.Drawing.Point(232, 2)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(21, 19)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "r"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(84, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(161, 20)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "You are logged in as:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImage = CType(resources.GetObject("PictureBox4.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.Location = New System.Drawing.Point(16, 11)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(64, 65)
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ZOSAssistantToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(222, 26)
        '
        'ZOSAssistantToolStripMenuItem
        '
        Me.ZOSAssistantToolStripMenuItem.Name = "ZOSAssistantToolStripMenuItem"
        Me.ZOSAssistantToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.ZOSAssistantToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.ZOSAssistantToolStripMenuItem.Text = "ZOS Assistant"
        '
        'Closable
        '
        Me.Closable.AutoSize = True
        Me.Closable.Location = New System.Drawing.Point(830, 279)
        Me.Closable.Name = "Closable"
        Me.Closable.Size = New System.Drawing.Size(15, 14)
        Me.Closable.TabIndex = 10
        Me.Closable.UseVisualStyleBackColor = True
        Me.Closable.Visible = False
        '
        'DesktopBackImage
        '
        Me.DesktopBackImage.Location = New System.Drawing.Point(-15, -15)
        Me.DesktopBackImage.Name = "DesktopBackImage"
        Me.DesktopBackImage.Size = New System.Drawing.Size(100, 50)
        Me.DesktopBackImage.TabIndex = 11
        Me.DesktopBackImage.TabStop = False
        Me.DesktopBackImage.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(331, 194)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(190, 191)
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'BatteryLevel
        '
        Me.BatteryLevel.Location = New System.Drawing.Point(-47, -6)
        Me.BatteryLevel.Name = "BatteryLevel"
        Me.BatteryLevel.Size = New System.Drawing.Size(100, 23)
        Me.BatteryLevel.TabIndex = 13
        Me.BatteryLevel.Visible = False
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.BackColor = System.Drawing.Color.White
        Me.ToolTip1.ForeColor = System.Drawing.Color.Black
        Me.ToolTip1.InitialDelay = 5
        Me.ToolTip1.ReshowDelay = 10
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.BackColor = System.Drawing.SystemColors.Highlight
        Me.Panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel8.Controls.Add(Me.Label8)
        Me.Panel8.Controls.Add(Me.Label9)
        Me.Panel8.Controls.Add(Me.Label10)
        Me.Panel8.Controls.Add(Me.PictureBox7)
        Me.Panel8.ForeColor = System.Drawing.Color.White
        Me.Panel8.Location = New System.Drawing.Point(589, 205)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(256, 91)
        Me.Panel8.TabIndex = 10
        Me.Panel8.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(84, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(157, 17)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = """Not yet implemented!""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Webdings", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label9.Location = New System.Drawing.Point(232, 2)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(21, 19)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "r"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(84, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(156, 19)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "An error has occured." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = CType(resources.GetObject("PictureBox7.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox7.Location = New System.Drawing.Point(16, 11)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(64, 65)
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'Panel12
        '
        Me.Panel12.AutoScroll = True
        Me.Panel12.Controls.Add(Me.TerminalIcon)
        Me.Panel12.Controls.Add(Me.LookChangerIcon)
        Me.Panel12.Controls.Add(Me.SettingsIcon)
        Me.Panel12.Controls.Add(Me.NotesIcon)
        Me.Panel12.Controls.Add(Me.GalleryIcon)
        Me.Panel12.Controls.Add(Me.CalculatorIcon)
        Me.Panel12.Controls.Add(Me.CalendarIcon)
        Me.Panel12.Controls.Add(Me.StoreIcon)
        Me.Panel12.Controls.Add(Me.RiPlayerIcon)
        Me.Panel12.Controls.Add(Me.ExplorerIcon)
        Me.Panel12.Controls.Add(Me.KittenFoxIcon)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel12.Location = New System.Drawing.Point(0, 0)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(453, 43)
        Me.Panel12.TabIndex = 11
        '
        'TerminalIcon
        '
        Me.TerminalIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Terminal
        Me.TerminalIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.TerminalIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.TerminalIcon.Location = New System.Drawing.Point(440, 0)
        Me.TerminalIcon.Name = "TerminalIcon"
        Me.TerminalIcon.Size = New System.Drawing.Size(44, 26)
        Me.TerminalIcon.TabIndex = 10
        Me.TerminalIcon.TabStop = False
        Me.TerminalIcon.Visible = False
        '
        'LookChangerIcon
        '
        Me.LookChangerIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Apparence
        Me.LookChangerIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.LookChangerIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.LookChangerIcon.Location = New System.Drawing.Point(396, 0)
        Me.LookChangerIcon.Name = "LookChangerIcon"
        Me.LookChangerIcon.Size = New System.Drawing.Size(44, 26)
        Me.LookChangerIcon.TabIndex = 9
        Me.LookChangerIcon.TabStop = False
        Me.LookChangerIcon.Visible = False
        '
        'SettingsIcon
        '
        Me.SettingsIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Parametre
        Me.SettingsIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.SettingsIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.SettingsIcon.Location = New System.Drawing.Point(352, 0)
        Me.SettingsIcon.Name = "SettingsIcon"
        Me.SettingsIcon.Size = New System.Drawing.Size(44, 26)
        Me.SettingsIcon.TabIndex = 8
        Me.SettingsIcon.TabStop = False
        Me.SettingsIcon.Visible = False
        '
        'NotesIcon
        '
        Me.NotesIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.NotePad
        Me.NotesIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.NotesIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.NotesIcon.Location = New System.Drawing.Point(308, 0)
        Me.NotesIcon.Name = "NotesIcon"
        Me.NotesIcon.Size = New System.Drawing.Size(44, 26)
        Me.NotesIcon.TabIndex = 7
        Me.NotesIcon.TabStop = False
        Me.NotesIcon.Visible = False
        '
        'GalleryIcon
        '
        Me.GalleryIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Photo
        Me.GalleryIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.GalleryIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.GalleryIcon.Location = New System.Drawing.Point(264, 0)
        Me.GalleryIcon.Name = "GalleryIcon"
        Me.GalleryIcon.Size = New System.Drawing.Size(44, 26)
        Me.GalleryIcon.TabIndex = 6
        Me.GalleryIcon.TabStop = False
        Me.GalleryIcon.Visible = False
        '
        'CalculatorIcon
        '
        Me.CalculatorIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Calculator
        Me.CalculatorIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.CalculatorIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.CalculatorIcon.Location = New System.Drawing.Point(220, 0)
        Me.CalculatorIcon.Name = "CalculatorIcon"
        Me.CalculatorIcon.Size = New System.Drawing.Size(44, 26)
        Me.CalculatorIcon.TabIndex = 5
        Me.CalculatorIcon.TabStop = False
        Me.CalculatorIcon.Visible = False
        '
        'CalendarIcon
        '
        Me.CalendarIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Calendrier_avec_Date
        Me.CalendarIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.CalendarIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.CalendarIcon.Location = New System.Drawing.Point(176, 0)
        Me.CalendarIcon.Name = "CalendarIcon"
        Me.CalendarIcon.Size = New System.Drawing.Size(44, 26)
        Me.CalendarIcon.TabIndex = 4
        Me.CalendarIcon.TabStop = False
        Me.CalendarIcon.Visible = False
        '
        'StoreIcon
        '
        Me.StoreIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.StoreIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.StoreIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.StoreIcon.Location = New System.Drawing.Point(132, 0)
        Me.StoreIcon.Name = "StoreIcon"
        Me.StoreIcon.Size = New System.Drawing.Size(44, 26)
        Me.StoreIcon.TabIndex = 3
        Me.StoreIcon.TabStop = False
        Me.StoreIcon.Visible = False
        '
        'RiPlayerIcon
        '
        Me.RiPlayerIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Movies
        Me.RiPlayerIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.RiPlayerIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.RiPlayerIcon.Location = New System.Drawing.Point(88, 0)
        Me.RiPlayerIcon.Name = "RiPlayerIcon"
        Me.RiPlayerIcon.Size = New System.Drawing.Size(44, 26)
        Me.RiPlayerIcon.TabIndex = 2
        Me.RiPlayerIcon.TabStop = False
        Me.RiPlayerIcon.Visible = False
        '
        'ExplorerIcon
        '
        Me.ExplorerIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.ExplorerIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ExplorerIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.ExplorerIcon.Location = New System.Drawing.Point(44, 0)
        Me.ExplorerIcon.Name = "ExplorerIcon"
        Me.ExplorerIcon.Size = New System.Drawing.Size(44, 26)
        Me.ExplorerIcon.TabIndex = 1
        Me.ExplorerIcon.TabStop = False
        Me.ExplorerIcon.Visible = False
        '
        'KittenFoxIcon
        '
        Me.KittenFoxIcon.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.KittenFoxIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.KittenFoxIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.KittenFoxIcon.Location = New System.Drawing.Point(0, 0)
        Me.KittenFoxIcon.Name = "KittenFoxIcon"
        Me.KittenFoxIcon.Size = New System.Drawing.Size(44, 26)
        Me.KittenFoxIcon.TabIndex = 0
        Me.KittenFoxIcon.TabStop = False
        Me.KittenFoxIcon.Visible = False
        '
        'StartScreen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(852, 579)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Closable)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.StepIndicator1)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.BatteryLevel)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.DesktopBackImage)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "StartScreen"
        Me.Text = "Desktop"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AppButton, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Userimagebutton, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WIFIIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.DesktopBackImage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        CType(Me.TerminalIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookChangerIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SettingsIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NotesIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GalleryIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CalculatorIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CalendarIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StoreIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RiPlayerIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExplorerIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KittenFoxIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents AppButton As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Userimagebutton As System.Windows.Forms.PictureBox
    Friend WithEvents StepIndicator1 As DevComponents.DotNetBar.Controls.StepIndicator
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Time As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents WIFIIcon As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents MonthCalendar1 As System.Windows.Forms.MonthCalendar
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ZOSAssistantToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Closable As System.Windows.Forms.CheckBox
    Friend WithEvents DesktopBackImage As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents BatteryLevel As System.Windows.Forms.ProgressBar
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents TerminalIcon As System.Windows.Forms.PictureBox
    Friend WithEvents LookChangerIcon As System.Windows.Forms.PictureBox
    Friend WithEvents SettingsIcon As System.Windows.Forms.PictureBox
    Friend WithEvents NotesIcon As System.Windows.Forms.PictureBox
    Friend WithEvents GalleryIcon As System.Windows.Forms.PictureBox
    Friend WithEvents CalculatorIcon As System.Windows.Forms.PictureBox
    Friend WithEvents CalendarIcon As System.Windows.Forms.PictureBox
    Friend WithEvents StoreIcon As System.Windows.Forms.PictureBox
    Friend WithEvents RiPlayerIcon As System.Windows.Forms.PictureBox
    Friend WithEvents ExplorerIcon As System.Windows.Forms.PictureBox
    Friend WithEvents KittenFoxIcon As System.Windows.Forms.PictureBox
End Class
