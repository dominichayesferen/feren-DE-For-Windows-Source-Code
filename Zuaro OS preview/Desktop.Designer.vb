﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Desktop
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Desktop))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.TerminalIcon = New System.Windows.Forms.PictureBox()
        Me.LookChangerIcon = New System.Windows.Forms.PictureBox()
        Me.SettingsIcon = New System.Windows.Forms.PictureBox()
        Me.NotesIcon = New System.Windows.Forms.PictureBox()
        Me.GalleryIcon = New System.Windows.Forms.PictureBox()
        Me.CalculatorIcon = New System.Windows.Forms.PictureBox()
        Me.CalendarIcon = New System.Windows.Forms.PictureBox()
        Me.StoreIcon = New System.Windows.Forms.PictureBox()
        Me.RiPlayerIcon = New System.Windows.Forms.PictureBox()
        Me.ExplorerIcon = New System.Windows.Forms.PictureBox()
        Me.Explorermenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.KittenFoxIcon = New System.Windows.Forms.PictureBox()
        Me.KittenFoxMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MaximizeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinimizeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AppButton = New System.Windows.Forms.PictureBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.WIFIIcon = New System.Windows.Forms.PictureBox()
        Me.Wifi = New System.Windows.Forms.CheckBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Userbutton = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Userimagebutton = New System.Windows.Forms.PictureBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.MusicName = New System.Windows.Forms.Label()
        Me.RatingStar1 = New DevComponents.DotNetBar.Controls.RatingStar()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel2size = New System.Windows.Forms.PictureBox()
        Me.Time = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.MonthCalendar()
        Me.buildtag = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.ContextMenuStrip3 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ChangeImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ZOSAssistantToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Closable = New System.Windows.Forms.CheckBox()
        Me.DesktopBackImage = New System.Windows.Forms.PictureBox()
        Me.BatteryLevel = New System.Windows.Forms.ProgressBar()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Stepindicator1 = New System.Windows.Forms.CheckBox()
        Me.Loading = New System.Windows.Forms.Timer(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.AutoProtection = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Slider2 = New DevComponents.DotNetBar.Controls.Slider()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Slider1 = New DevComponents.DotNetBar.Controls.Slider()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.BubbleBar1 = New DevComponents.DotNetBar.BubbleBar()
        Me.BubbleBarTab1 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
        Me.BubbleButton1 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton2 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton3 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton4 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton5 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton6 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton9 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton10 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton7 = New DevComponents.DotNetBar.BubbleButton()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Deskmenu = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.TerminalIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookChangerIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SettingsIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NotesIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GalleryIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CalculatorIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CalendarIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StoreIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RiPlayerIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExplorerIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Explorermenu.SuspendLayout()
        CType(Me.KittenFoxIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.KittenFoxMenu.SuspendLayout()
        CType(Me.AppButton, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        CType(Me.WIFIIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Userbutton, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Userimagebutton, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.Panel2size, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.DesktopBackImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel14.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel15.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel17.SuspendLayout()
        Me.Panel18.SuspendLayout()
        CType(Me.BubbleBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Deskmenu.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = CType(resources.GetObject("Panel1.BackgroundImage"), System.Drawing.Image)
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.AppButton)
        Me.Panel1.Controls.Add(Me.Panel9)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 536)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(853, 43)
        Me.Panel1.TabIndex = 0
        Me.Panel1.Visible = False
        '
        'Panel11
        '
        Me.Panel11.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel11.AutoScroll = True
        Me.Panel11.Controls.Add(Me.TerminalIcon)
        Me.Panel11.Controls.Add(Me.LookChangerIcon)
        Me.Panel11.Controls.Add(Me.SettingsIcon)
        Me.Panel11.Controls.Add(Me.NotesIcon)
        Me.Panel11.Controls.Add(Me.GalleryIcon)
        Me.Panel11.Controls.Add(Me.CalculatorIcon)
        Me.Panel11.Controls.Add(Me.CalendarIcon)
        Me.Panel11.Controls.Add(Me.StoreIcon)
        Me.Panel11.Controls.Add(Me.RiPlayerIcon)
        Me.Panel11.Controls.Add(Me.ExplorerIcon)
        Me.Panel11.Controls.Add(Me.KittenFoxIcon)
        Me.Panel11.Location = New System.Drawing.Point(53, 1)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(576, 40)
        Me.Panel11.TabIndex = 10
        '
        'TerminalIcon
        '
        Me.TerminalIcon.BackgroundImage = CType(resources.GetObject("TerminalIcon.BackgroundImage"), System.Drawing.Image)
        Me.TerminalIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.TerminalIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.TerminalIcon.Image = CType(resources.GetObject("TerminalIcon.Image"), System.Drawing.Image)
        Me.TerminalIcon.Location = New System.Drawing.Point(520, 0)
        Me.TerminalIcon.Name = "TerminalIcon"
        Me.TerminalIcon.Size = New System.Drawing.Size(52, 40)
        Me.TerminalIcon.TabIndex = 10
        Me.TerminalIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.TerminalIcon, "Terminal")
        Me.TerminalIcon.Visible = False
        '
        'LookChangerIcon
        '
        Me.LookChangerIcon.BackgroundImage = CType(resources.GetObject("LookChangerIcon.BackgroundImage"), System.Drawing.Image)
        Me.LookChangerIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.LookChangerIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.LookChangerIcon.Image = CType(resources.GetObject("LookChangerIcon.Image"), System.Drawing.Image)
        Me.LookChangerIcon.Location = New System.Drawing.Point(468, 0)
        Me.LookChangerIcon.Name = "LookChangerIcon"
        Me.LookChangerIcon.Size = New System.Drawing.Size(52, 40)
        Me.LookChangerIcon.TabIndex = 9
        Me.LookChangerIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.LookChangerIcon, "Zuaro OS Look Changer")
        Me.LookChangerIcon.Visible = False
        '
        'SettingsIcon
        '
        Me.SettingsIcon.BackgroundImage = CType(resources.GetObject("SettingsIcon.BackgroundImage"), System.Drawing.Image)
        Me.SettingsIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.SettingsIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.SettingsIcon.Image = CType(resources.GetObject("SettingsIcon.Image"), System.Drawing.Image)
        Me.SettingsIcon.Location = New System.Drawing.Point(416, 0)
        Me.SettingsIcon.Name = "SettingsIcon"
        Me.SettingsIcon.Size = New System.Drawing.Size(52, 40)
        Me.SettingsIcon.TabIndex = 8
        Me.SettingsIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.SettingsIcon, "Settings")
        Me.SettingsIcon.Visible = False
        '
        'NotesIcon
        '
        Me.NotesIcon.BackgroundImage = CType(resources.GetObject("NotesIcon.BackgroundImage"), System.Drawing.Image)
        Me.NotesIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.NotesIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.NotesIcon.Image = CType(resources.GetObject("NotesIcon.Image"), System.Drawing.Image)
        Me.NotesIcon.Location = New System.Drawing.Point(364, 0)
        Me.NotesIcon.Name = "NotesIcon"
        Me.NotesIcon.Size = New System.Drawing.Size(52, 40)
        Me.NotesIcon.TabIndex = 7
        Me.NotesIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.NotesIcon, "Notes")
        Me.NotesIcon.Visible = False
        '
        'GalleryIcon
        '
        Me.GalleryIcon.BackgroundImage = CType(resources.GetObject("GalleryIcon.BackgroundImage"), System.Drawing.Image)
        Me.GalleryIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.GalleryIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.GalleryIcon.Image = CType(resources.GetObject("GalleryIcon.Image"), System.Drawing.Image)
        Me.GalleryIcon.Location = New System.Drawing.Point(312, 0)
        Me.GalleryIcon.Name = "GalleryIcon"
        Me.GalleryIcon.Size = New System.Drawing.Size(52, 40)
        Me.GalleryIcon.TabIndex = 6
        Me.GalleryIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.GalleryIcon, "Gallery")
        Me.GalleryIcon.Visible = False
        '
        'CalculatorIcon
        '
        Me.CalculatorIcon.BackgroundImage = CType(resources.GetObject("CalculatorIcon.BackgroundImage"), System.Drawing.Image)
        Me.CalculatorIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.CalculatorIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.CalculatorIcon.Image = CType(resources.GetObject("CalculatorIcon.Image"), System.Drawing.Image)
        Me.CalculatorIcon.Location = New System.Drawing.Point(260, 0)
        Me.CalculatorIcon.Name = "CalculatorIcon"
        Me.CalculatorIcon.Size = New System.Drawing.Size(52, 40)
        Me.CalculatorIcon.TabIndex = 5
        Me.CalculatorIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.CalculatorIcon, "Calculator")
        Me.CalculatorIcon.Visible = False
        '
        'CalendarIcon
        '
        Me.CalendarIcon.BackgroundImage = CType(resources.GetObject("CalendarIcon.BackgroundImage"), System.Drawing.Image)
        Me.CalendarIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.CalendarIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.CalendarIcon.Image = CType(resources.GetObject("CalendarIcon.Image"), System.Drawing.Image)
        Me.CalendarIcon.Location = New System.Drawing.Point(208, 0)
        Me.CalendarIcon.Name = "CalendarIcon"
        Me.CalendarIcon.Size = New System.Drawing.Size(52, 40)
        Me.CalendarIcon.TabIndex = 4
        Me.CalendarIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.CalendarIcon, "Calendar")
        Me.CalendarIcon.Visible = False
        '
        'StoreIcon
        '
        Me.StoreIcon.BackgroundImage = CType(resources.GetObject("StoreIcon.BackgroundImage"), System.Drawing.Image)
        Me.StoreIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.StoreIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.StoreIcon.Image = CType(resources.GetObject("StoreIcon.Image"), System.Drawing.Image)
        Me.StoreIcon.Location = New System.Drawing.Point(156, 0)
        Me.StoreIcon.Name = "StoreIcon"
        Me.StoreIcon.Size = New System.Drawing.Size(52, 40)
        Me.StoreIcon.TabIndex = 3
        Me.StoreIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.StoreIcon, "Store")
        Me.StoreIcon.Visible = False
        '
        'RiPlayerIcon
        '
        Me.RiPlayerIcon.BackgroundImage = CType(resources.GetObject("RiPlayerIcon.BackgroundImage"), System.Drawing.Image)
        Me.RiPlayerIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.RiPlayerIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.RiPlayerIcon.Image = CType(resources.GetObject("RiPlayerIcon.Image"), System.Drawing.Image)
        Me.RiPlayerIcon.Location = New System.Drawing.Point(104, 0)
        Me.RiPlayerIcon.Name = "RiPlayerIcon"
        Me.RiPlayerIcon.Size = New System.Drawing.Size(52, 40)
        Me.RiPlayerIcon.TabIndex = 2
        Me.RiPlayerIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.RiPlayerIcon, "Ri Player")
        Me.RiPlayerIcon.Visible = False
        '
        'ExplorerIcon
        '
        Me.ExplorerIcon.BackgroundImage = CType(resources.GetObject("ExplorerIcon.BackgroundImage"), System.Drawing.Image)
        Me.ExplorerIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ExplorerIcon.ContextMenuStrip = Me.Explorermenu
        Me.ExplorerIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.ExplorerIcon.Image = CType(resources.GetObject("ExplorerIcon.Image"), System.Drawing.Image)
        Me.ExplorerIcon.Location = New System.Drawing.Point(52, 0)
        Me.ExplorerIcon.Name = "ExplorerIcon"
        Me.ExplorerIcon.Size = New System.Drawing.Size(52, 40)
        Me.ExplorerIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ExplorerIcon.TabIndex = 1
        Me.ExplorerIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.ExplorerIcon, "Explorer")
        Me.ExplorerIcon.Visible = False
        '
        'Explorermenu
        '
        Me.Explorermenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem3, Me.ToolStripMenuItem4, Me.ToolStripMenuItem5})
        Me.Explorermenu.Name = "KittenFoxMenu"
        Me.Explorermenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.Explorermenu.Size = New System.Drawing.Size(125, 70)
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(124, 22)
        Me.ToolStripMenuItem3.Text = "Close"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(124, 22)
        Me.ToolStripMenuItem4.Text = "Maximize"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(124, 22)
        Me.ToolStripMenuItem5.Text = "Restore"
        '
        'KittenFoxIcon
        '
        Me.KittenFoxIcon.BackgroundImage = CType(resources.GetObject("KittenFoxIcon.BackgroundImage"), System.Drawing.Image)
        Me.KittenFoxIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.KittenFoxIcon.ContextMenuStrip = Me.KittenFoxMenu
        Me.KittenFoxIcon.Dock = System.Windows.Forms.DockStyle.Left
        Me.KittenFoxIcon.Image = CType(resources.GetObject("KittenFoxIcon.Image"), System.Drawing.Image)
        Me.KittenFoxIcon.Location = New System.Drawing.Point(0, 0)
        Me.KittenFoxIcon.Name = "KittenFoxIcon"
        Me.KittenFoxIcon.Size = New System.Drawing.Size(52, 40)
        Me.KittenFoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.KittenFoxIcon.TabIndex = 0
        Me.KittenFoxIcon.TabStop = False
        Me.ToolTip1.SetToolTip(Me.KittenFoxIcon, "KittenFox")
        Me.KittenFoxIcon.Visible = False
        '
        'KittenFoxMenu
        '
        Me.KittenFoxMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem12, Me.SettingsToolStripMenuItem, Me.ToolStripSeparator3, Me.CloseToolStripMenuItem, Me.MaximizeToolStripMenuItem, Me.MinimizeToolStripMenuItem})
        Me.KittenFoxMenu.Name = "KittenFoxMenu"
        Me.KittenFoxMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.KittenFoxMenu.Size = New System.Drawing.Size(189, 120)
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(188, 22)
        Me.ToolStripMenuItem12.Text = "New Window"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(185, 6)
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'MaximizeToolStripMenuItem
        '
        Me.MaximizeToolStripMenuItem.Name = "MaximizeToolStripMenuItem"
        Me.MaximizeToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.MaximizeToolStripMenuItem.Text = "Maximize"
        '
        'MinimizeToolStripMenuItem
        '
        Me.MinimizeToolStripMenuItem.Name = "MinimizeToolStripMenuItem"
        Me.MinimizeToolStripMenuItem.Size = New System.Drawing.Size(188, 22)
        Me.MinimizeToolStripMenuItem.Text = "Restore"
        '
        'AppButton
        '
        Me.AppButton.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.AppButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.AppButton.Image = CType(resources.GetObject("AppButton.Image"), System.Drawing.Image)
        Me.AppButton.Location = New System.Drawing.Point(0, 3)
        Me.AppButton.Name = "AppButton"
        Me.AppButton.Size = New System.Drawing.Size(53, 37)
        Me.AppButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AppButton.TabIndex = 0
        Me.AppButton.TabStop = False
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.WIFIIcon)
        Me.Panel9.Controls.Add(Me.Wifi)
        Me.Panel9.Controls.Add(Me.PictureBox2)
        Me.Panel9.Controls.Add(Me.Userbutton)
        Me.Panel9.Controls.Add(Me.PictureBox8)
        Me.Panel9.Controls.Add(Me.PictureBox5)
        Me.Panel9.Controls.Add(Me.PictureBox9)
        Me.Panel9.Controls.Add(Me.PictureBox3)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel9.Location = New System.Drawing.Point(635, 0)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(218, 43)
        Me.Panel9.TabIndex = 8
        '
        'WIFIIcon
        '
        Me.WIFIIcon.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.WIFIIcon.Image = CType(resources.GetObject("WIFIIcon.Image"), System.Drawing.Image)
        Me.WIFIIcon.Location = New System.Drawing.Point(97, 12)
        Me.WIFIIcon.Name = "WIFIIcon"
        Me.WIFIIcon.Size = New System.Drawing.Size(24, 25)
        Me.WIFIIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.WIFIIcon.TabIndex = 3
        Me.WIFIIcon.TabStop = False
        '
        'Wifi
        '
        Me.Wifi.AutoSize = True
        Me.Wifi.Location = New System.Drawing.Point(100, 9)
        Me.Wifi.Name = "Wifi"
        Me.Wifi.Size = New System.Drawing.Size(15, 14)
        Me.Wifi.TabIndex = 9
        Me.Wifi.UseVisualStyleBackColor = True
        Me.Wifi.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(39, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(23, 25)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = False
        '
        'Userbutton
        '
        Me.Userbutton.BackgroundImage = CType(resources.GetObject("Userbutton.BackgroundImage"), System.Drawing.Image)
        Me.Userbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Userbutton.Location = New System.Drawing.Point(187, 12)
        Me.Userbutton.Name = "Userbutton"
        Me.Userbutton.Size = New System.Drawing.Size(24, 25)
        Me.Userbutton.TabIndex = 2
        Me.Userbutton.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox8.BackgroundImage = CType(resources.GetObject("PictureBox8.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox8.Location = New System.Drawing.Point(-3, 8)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(36, 32)
        Me.PictureBox8.TabIndex = 7
        Me.PictureBox8.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox5.BackgroundImage = CType(resources.GetObject("PictureBox5.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox5.Location = New System.Drawing.Point(127, 12)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(24, 25)
        Me.PictureBox5.TabIndex = 4
        Me.PictureBox5.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox9.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(68, 12)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(23, 25)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox9.TabIndex = 6
        Me.PictureBox9.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.PictureBox3.BackgroundImage = CType(resources.GetObject("PictureBox3.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox3.Location = New System.Drawing.Point(157, 12)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(24, 25)
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'Userimagebutton
        '
        Me.Userimagebutton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Userimagebutton.Image = CType(resources.GetObject("Userimagebutton.Image"), System.Drawing.Image)
        Me.Userimagebutton.Location = New System.Drawing.Point(14, 12)
        Me.Userimagebutton.Name = "Userimagebutton"
        Me.Userimagebutton.Size = New System.Drawing.Size(56, 54)
        Me.Userimagebutton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Userimagebutton.TabIndex = 1
        Me.Userimagebutton.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.BackgroundImage = CType(resources.GetObject("Panel10.BackgroundImage"), System.Drawing.Image)
        Me.Panel10.Controls.Add(Me.PictureBox19)
        Me.Panel10.Controls.Add(Me.MusicName)
        Me.Panel10.Controls.Add(Me.RatingStar1)
        Me.Panel10.Controls.Add(Me.ProgressBar1)
        Me.Panel10.Controls.Add(Me.PictureBox4)
        Me.Panel10.Controls.Add(Me.Label13)
        Me.Panel10.Controls.Add(Me.Label15)
        Me.Panel10.Controls.Add(Me.Label12)
        Me.Panel10.Controls.Add(Me.Label14)
        Me.Panel10.Location = New System.Drawing.Point(519, 381)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(296, 166)
        Me.Panel10.TabIndex = 9
        Me.Panel10.Visible = False
        '
        'PictureBox19
        '
        Me.PictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox19.Image = CType(resources.GetObject("PictureBox19.Image"), System.Drawing.Image)
        Me.PictureBox19.Location = New System.Drawing.Point(-17, 156)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(10, 10)
        Me.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox19.TabIndex = 22
        Me.PictureBox19.TabStop = False
        '
        'MusicName
        '
        Me.MusicName.AutoEllipsis = True
        Me.MusicName.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MusicName.ForeColor = System.Drawing.Color.White
        Me.MusicName.Location = New System.Drawing.Point(133, 35)
        Me.MusicName.Name = "MusicName"
        Me.MusicName.Size = New System.Drawing.Size(150, 23)
        Me.MusicName.TabIndex = 21
        Me.MusicName.Text = "Track Name"
        '
        'RatingStar1
        '
        '
        '
        '
        Me.RatingStar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RatingStar1.Location = New System.Drawing.Point(132, 95)
        Me.RatingStar1.Name = "RatingStar1"
        Me.RatingStar1.Size = New System.Drawing.Size(75, 23)
        Me.RatingStar1.TabIndex = 16
        Me.RatingStar1.TextColor = System.Drawing.Color.Empty
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(128, 77)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(155, 12)
        Me.ProgressBar1.TabIndex = 20
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(14, 13)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(108, 104)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 19
        Me.PictureBox4.TabStop = False
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(90, 124)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 32)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = ""
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(131, 124)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 32)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = ""
        Me.Label15.Visible = False
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(131, 124)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 32)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = ""
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(172, 124)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 32)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = ""
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = CType(resources.GetObject("Panel2.BackgroundImage"), System.Drawing.Image)
        Me.Panel2.Controls.Add(Me.Panel2size)
        Me.Panel2.Controls.Add(Me.Time)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Panel2.Location = New System.Drawing.Point(230, 86)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(241, 78)
        Me.Panel2.TabIndex = 1
        Me.Panel2.Visible = False
        '
        'Panel2size
        '
        Me.Panel2size.Location = New System.Drawing.Point(250, 16)
        Me.Panel2size.Name = "Panel2size"
        Me.Panel2size.Size = New System.Drawing.Size(241, 98)
        Me.Panel2size.TabIndex = 2
        Me.Panel2size.TabStop = False
        Me.Panel2size.Visible = False
        '
        'Time
        '
        Me.Time.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Time.BackColor = System.Drawing.Color.Transparent
        Me.Time.Cursor = System.Windows.Forms.Cursors.Default
        Me.Time.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Time.ForeColor = System.Drawing.Color.White
        Me.Time.Location = New System.Drawing.Point(3, 13)
        Me.Time.Name = "Time"
        Me.Time.Size = New System.Drawing.Size(121, 54)
        Me.Time.TabIndex = 0
        Me.Time.Text = "00:00:00"
        Me.Time.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Label4.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(117, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(127, 66)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "dd/mm/yyyy"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Location = New System.Drawing.Point(230, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.TabIndex = 0
        Me.Panel5.Visible = False
        '
        'buildtag
        '
        Me.buildtag.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buildtag.AutoSize = True
        Me.buildtag.BackColor = System.Drawing.Color.Transparent
        Me.buildtag.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buildtag.ForeColor = System.Drawing.Color.White
        Me.buildtag.Location = New System.Drawing.Point(680, 473)
        Me.buildtag.Name = "buildtag"
        Me.buildtag.Size = New System.Drawing.Size(161, 60)
        Me.buildtag.TabIndex = 4
        Me.buildtag.Text = "Zuaro OS Beta 1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Build v.1.0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "zuaroos.weebly.com"
        Me.buildtag.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.BackgroundImage = CType(resources.GetObject("Panel3.BackgroundImage"), System.Drawing.Image)
        Me.Panel3.Controls.Add(Me.PictureBox23)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Location = New System.Drawing.Point(659, 174)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(194, 363)
        Me.Panel3.TabIndex = 5
        Me.Panel3.Visible = False
        '
        'PictureBox23
        '
        Me.PictureBox23.BackgroundImage = CType(resources.GetObject("PictureBox23.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox23.ContextMenuStrip = Me.ContextMenuStrip3
        Me.PictureBox23.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox23.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(194, 111)
        Me.PictureBox23.TabIndex = 1
        Me.PictureBox23.TabStop = False
        '
        'ContextMenuStrip3
        '
        Me.ContextMenuStrip3.BackColor = System.Drawing.Color.White
        Me.ContextMenuStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChangeImageToolStripMenuItem})
        Me.ContextMenuStrip3.Name = "ContextMenuStrip3"
        Me.ContextMenuStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ContextMenuStrip3.ShowImageMargin = False
        Me.ContextMenuStrip3.Size = New System.Drawing.Size(136, 26)
        '
        'ChangeImageToolStripMenuItem
        '
        Me.ChangeImageToolStripMenuItem.Name = "ChangeImageToolStripMenuItem"
        Me.ChangeImageToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.ChangeImageToolStripMenuItem.Text = "Change image..."
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.Controls.Add(Me.PictureBox22)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 111)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(194, 252)
        Me.Panel4.TabIndex = 0
        '
        'PictureBox22
        '
        Me.PictureBox22.BackgroundImage = CType(resources.GetObject("PictureBox22.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox22.Image = CType(resources.GetObject("PictureBox22.Image"), System.Drawing.Image)
        Me.PictureBox22.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(49, 49)
        Me.PictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox22.TabIndex = 2
        Me.PictureBox22.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoEllipsis = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(54, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(140, 33)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "User1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Timer1
        '
        Me.Timer1.Interval = 50
        '
        'Timer2
        '
        Me.Timer2.Interval = 3000
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ZOSAssistantToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(213, 26)
        '
        'ZOSAssistantToolStripMenuItem
        '
        Me.ZOSAssistantToolStripMenuItem.Name = "ZOSAssistantToolStripMenuItem"
        Me.ZOSAssistantToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
            Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.ZOSAssistantToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.ZOSAssistantToolStripMenuItem.Text = "ZOS Assistant"
        '
        'Closable
        '
        Me.Closable.AutoSize = True
        Me.Closable.Location = New System.Drawing.Point(866, 279)
        Me.Closable.Name = "Closable"
        Me.Closable.Size = New System.Drawing.Size(15, 14)
        Me.Closable.TabIndex = 10
        Me.Closable.UseVisualStyleBackColor = True
        Me.Closable.Visible = False
        '
        'DesktopBackImage
        '
        Me.DesktopBackImage.Location = New System.Drawing.Point(-15, -15)
        Me.DesktopBackImage.Name = "DesktopBackImage"
        Me.DesktopBackImage.Size = New System.Drawing.Size(100, 50)
        Me.DesktopBackImage.TabIndex = 11
        Me.DesktopBackImage.TabStop = False
        Me.DesktopBackImage.Visible = False
        '
        'BatteryLevel
        '
        Me.BatteryLevel.Location = New System.Drawing.Point(-47, -6)
        Me.BatteryLevel.Name = "BatteryLevel"
        Me.BatteryLevel.Size = New System.Drawing.Size(100, 23)
        Me.BatteryLevel.TabIndex = 13
        Me.BatteryLevel.Visible = False
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.BackColor = System.Drawing.Color.White
        Me.ToolTip1.ForeColor = System.Drawing.Color.Black
        Me.ToolTip1.InitialDelay = 5
        Me.ToolTip1.ReshowDelay = 10
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI Symbol", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(161, 415)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(101, 30)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = ""
        Me.Label11.Visible = False
        '
        'Stepindicator1
        '
        Me.Stepindicator1.AutoSize = True
        Me.Stepindicator1.Location = New System.Drawing.Point(-15, -18)
        Me.Stepindicator1.Name = "Stepindicator1"
        Me.Stepindicator1.Size = New System.Drawing.Size(81, 17)
        Me.Stepindicator1.TabIndex = 15
        Me.Stepindicator1.Text = "CheckBox1"
        Me.Stepindicator1.UseVisualStyleBackColor = True
        Me.Stepindicator1.Visible = False
        '
        'Loading
        '
        Me.Loading.Interval = 1000
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.PictureBox16)
        Me.Panel6.Controls.Add(Me.PictureBox15)
        Me.Panel6.Controls.Add(Me.Panel14)
        Me.Panel6.Controls.Add(Me.PictureBox14)
        Me.Panel6.Controls.Add(Me.PictureBox13)
        Me.Panel6.Controls.Add(Me.Panel13)
        Me.Panel6.Controls.Add(Me.Panel12)
        Me.Panel6.Controls.Add(Me.PictureBox12)
        Me.Panel6.Controls.Add(Me.Panel8)
        Me.Panel6.Controls.Add(Me.Panel7)
        Me.Panel6.Location = New System.Drawing.Point(609, 244)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(232, 293)
        Me.Panel6.TabIndex = 16
        Me.Panel6.Visible = False
        '
        'PictureBox16
        '
        Me.PictureBox16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox16.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PictureBox16.Location = New System.Drawing.Point(0, 237)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(232, 1)
        Me.PictureBox16.TabIndex = 9
        Me.PictureBox16.TabStop = False
        '
        'PictureBox15
        '
        Me.PictureBox15.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox15.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox15.Location = New System.Drawing.Point(0, 225)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(232, 1)
        Me.PictureBox15.TabIndex = 8
        Me.PictureBox15.TabStop = False
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.Label16)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel14.Location = New System.Drawing.Point(0, 171)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(232, 54)
        Me.Panel14.TabIndex = 6
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(16, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(191, 22)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Launch ZUARO Defender"
        '
        'PictureBox14
        '
        Me.PictureBox14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox14.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox14.Location = New System.Drawing.Point(0, 170)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(232, 1)
        Me.PictureBox14.TabIndex = 7
        Me.PictureBox14.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox13.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PictureBox13.Location = New System.Drawing.Point(0, 238)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(232, 1)
        Me.PictureBox13.TabIndex = 4
        Me.PictureBox13.TabStop = False
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Label10)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel13.Location = New System.Drawing.Point(0, 239)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(232, 54)
        Me.Panel13.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(16, 18)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(142, 22)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Turn Off Defender"
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Label8)
        Me.Panel12.Controls.Add(Me.Label7)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel12.Location = New System.Drawing.Point(0, 116)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(232, 54)
        Me.Panel12.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.DimGray
        Me.Label8.Location = New System.Drawing.Point(17, 29)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 18)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Never Scanned"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(16, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(117, 22)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Last Scan Area:"
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox12.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox12.Location = New System.Drawing.Point(0, 115)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(232, 1)
        Me.PictureBox12.TabIndex = 2
        Me.PictureBox12.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.PictureBox11)
        Me.Panel8.Controls.Add(Me.AutoProtection)
        Me.Panel8.Controls.Add(Me.Label6)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(0, 61)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(232, 54)
        Me.Panel8.TabIndex = 1
        '
        'PictureBox11
        '
        Me.PictureBox11.BackgroundImage = CType(resources.GetObject("PictureBox11.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox11.Location = New System.Drawing.Point(170, 12)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(51, 31)
        Me.PictureBox11.TabIndex = 0
        Me.PictureBox11.TabStop = False
        '
        'AutoProtection
        '
        Me.AutoProtection.AutoSize = True
        Me.AutoProtection.Location = New System.Drawing.Point(183, 20)
        Me.AutoProtection.Name = "AutoProtection"
        Me.AutoProtection.Size = New System.Drawing.Size(15, 14)
        Me.AutoProtection.TabIndex = 2
        Me.AutoProtection.UseVisualStyleBackColor = True
        Me.AutoProtection.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(16, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(124, 22)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Auto Protection"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.DodgerBlue
        Me.Panel7.Controls.Add(Me.Label5)
        Me.Panel7.Controls.Add(Me.PictureBox10)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(232, 61)
        Me.Panel7.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(66, 19)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(163, 23)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Unknown"
        '
        'PictureBox10
        '
        Me.PictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(6, 6)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(54, 50)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox10.TabIndex = 0
        Me.PictureBox10.TabStop = False
        '
        'Panel15
        '
        Me.Panel15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel15.BackColor = System.Drawing.Color.White
        Me.Panel15.Controls.Add(Me.PictureBox17)
        Me.Panel15.Controls.Add(Me.Slider2)
        Me.Panel15.Controls.Add(Me.PictureBox7)
        Me.Panel15.Controls.Add(Me.Slider1)
        Me.Panel15.Controls.Add(Me.Label9)
        Me.Panel15.Location = New System.Drawing.Point(553, 361)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(293, 186)
        Me.Panel15.TabIndex = 17
        Me.Panel15.Visible = False
        '
        'PictureBox17
        '
        Me.PictureBox17.BackgroundImage = CType(resources.GetObject("PictureBox17.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox17.Location = New System.Drawing.Point(17, 114)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(51, 45)
        Me.PictureBox17.TabIndex = 2
        Me.PictureBox17.TabStop = False
        '
        'Slider2
        '
        '
        '
        '
        Me.Slider2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Slider2.Enabled = False
        Me.Slider2.Location = New System.Drawing.Point(42, 127)
        Me.Slider2.Name = "Slider2"
        Me.Slider2.Size = New System.Drawing.Size(243, 23)
        Me.Slider2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.Slider2.TabIndex = 4
        Me.Slider2.Value = 0
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = CType(resources.GetObject("PictureBox7.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox7.Location = New System.Drawing.Point(17, 56)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(51, 45)
        Me.PictureBox7.TabIndex = 1
        Me.PictureBox7.TabStop = False
        '
        'Slider1
        '
        '
        '
        '
        Me.Slider1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Slider1.Location = New System.Drawing.Point(42, 68)
        Me.Slider1.Name = "Slider1"
        Me.Slider1.Size = New System.Drawing.Size(243, 23)
        Me.Slider1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.Slider1.TabIndex = 3
        Me.Slider1.Value = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 14.25!)
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(10, 12)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 26)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Volume"
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.Black)
        '
        'Panel16
        '
        Me.Panel16.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel16.BackColor = System.Drawing.Color.White
        Me.Panel16.Controls.Add(Me.LinkLabel1)
        Me.Panel16.Controls.Add(Me.Label18)
        Me.Panel16.Controls.Add(Me.PictureBox18)
        Me.Panel16.Controls.Add(Me.Label17)
        Me.Panel16.Location = New System.Drawing.Point(544, 410)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(293, 137)
        Me.Panel16.TabIndex = 18
        Me.Panel16.Visible = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.ActiveLinkColor = System.Drawing.Color.SteelBlue
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LinkLabel1.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.LinkLabel1.LinkColor = System.Drawing.Color.SteelBlue
        Me.LinkLabel1.Location = New System.Drawing.Point(88, 91)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(106, 16)
        Me.LinkLabel1.TabIndex = 3
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Go to Connections"
        Me.LinkLabel1.VisitedLinkColor = System.Drawing.Color.SteelBlue
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Red
        Me.Label18.Location = New System.Drawing.Point(86, 62)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(139, 26)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "Not Connected"
        '
        'PictureBox18
        '
        Me.PictureBox18.BackgroundImage = CType(resources.GetObject("PictureBox18.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox18.Location = New System.Drawing.Point(15, 51)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(64, 62)
        Me.PictureBox18.TabIndex = 1
        Me.PictureBox18.TabStop = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 14.25!)
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(10, 12)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(84, 26)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Network"
        '
        'Panel17
        '
        Me.Panel17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel17.BackColor = System.Drawing.Color.Transparent
        Me.Panel17.BackgroundImage = CType(resources.GetObject("Panel17.BackgroundImage"), System.Drawing.Image)
        Me.Panel17.Controls.Add(Me.Label19)
        Me.Panel17.Controls.Add(Me.Label3)
        Me.Panel17.Controls.Add(Me.Userimagebutton)
        Me.Panel17.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.Panel17.Location = New System.Drawing.Point(2, 86)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(222, 79)
        Me.Panel17.TabIndex = 19
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(82, 46)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(55, 17)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Log Out"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(78, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 32)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "User1"
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.Transparent
        Me.Panel18.Controls.Add(Me.Panel5)
        Me.Panel18.Controls.Add(Me.Panel2)
        Me.Panel18.Controls.Add(Me.Panel17)
        Me.Panel18.Location = New System.Drawing.Point(382, 384)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(471, 165)
        Me.Panel18.TabIndex = 20
        '
        'BubbleBar1
        '
        Me.BubbleBar1.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom
        Me.BubbleBar1.AntiAlias = True
        Me.BubbleBar1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.BubbleBar1.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.BubbleBar1.BackgroundStyle.BackColorGradientType = DevComponents.DotNetBar.eGradientType.Radial
        Me.BubbleBar1.BackgroundStyle.BorderBottomColor = System.Drawing.Color.Transparent
        Me.BubbleBar1.BackgroundStyle.BorderColor = System.Drawing.Color.Transparent
        Me.BubbleBar1.BackgroundStyle.BorderTopColor = System.Drawing.Color.Transparent
        Me.BubbleBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.BubbleBar1.ButtonBackAreaStyle.BackColor = System.Drawing.Color.Transparent
        Me.BubbleBar1.ButtonBackAreaStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.BubbleBar1.ButtonBackAreaStyle.BorderBottomWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.BubbleBar1.ButtonBackAreaStyle.BorderLeftWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.BorderRightWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.BorderTopWidth = 1
        Me.BubbleBar1.ButtonBackAreaStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingBottom = 3
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingLeft = 3
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingRight = 3
        Me.BubbleBar1.ButtonBackAreaStyle.PaddingTop = 3
        Me.BubbleBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BubbleBar1.ImageSizeLarge = New System.Drawing.Size(50, 50)
        Me.BubbleBar1.ImageSizeNormal = New System.Drawing.Size(48, 48)
        Me.BubbleBar1.Location = New System.Drawing.Point(0, 481)
        Me.BubbleBar1.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight
        Me.BubbleBar1.Name = "BubbleBar1"
        Me.BubbleBar1.SelectedTab = Me.BubbleBarTab1
        Me.BubbleBar1.SelectedTabColors.BorderColor = System.Drawing.Color.Black
        Me.BubbleBar1.Size = New System.Drawing.Size(853, 55)
        Me.BubbleBar1.TabIndex = 24
        Me.BubbleBar1.Tabs.Add(Me.BubbleBarTab1)
        Me.BubbleBar1.Text = "BubbleBar1"
        Me.BubbleBar1.Visible = False
        '
        'BubbleBarTab1
        '
        Me.BubbleBarTab1.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.BubbleBarTab1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.BubbleBarTab1.Buttons.AddRange(New DevComponents.DotNetBar.BubbleButton() {Me.BubbleButton1, Me.BubbleButton2, Me.BubbleButton3, Me.BubbleButton4, Me.BubbleButton5, Me.BubbleButton6, Me.BubbleButton9, Me.BubbleButton10, Me.BubbleButton7})
        Me.BubbleBarTab1.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab1.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BubbleBarTab1.Name = "BubbleBarTab1"
        Me.BubbleBarTab1.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Blue
        Me.BubbleBarTab1.Text = "BubbleBarTab1"
        Me.BubbleBarTab1.TextColor = System.Drawing.Color.Black
        '
        'BubbleButton1
        '
        Me.BubbleButton1.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.BubbleButton1.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.BubbleButton1.Name = "BubbleButton1"
        Me.BubbleButton1.TooltipText = "KittenFox"
        '
        'BubbleButton2
        '
        Me.BubbleButton2.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.BubbleButton2.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.BubbleButton2.Name = "BubbleButton2"
        Me.BubbleButton2.TooltipText = "Explorer"
        '
        'BubbleButton3
        '
        Me.BubbleButton3.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.BubbleButton3.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.BubbleButton3.Name = "BubbleButton3"
        Me.BubbleButton3.TooltipText = "Store"
        '
        'BubbleButton4
        '
        Me.BubbleButton4.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Movies
        Me.BubbleButton4.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Movies
        Me.BubbleButton4.Name = "BubbleButton4"
        Me.BubbleButton4.TooltipText = "Ri Player"
        '
        'BubbleButton5
        '
        Me.BubbleButton5.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Calendrier_avec_Date
        Me.BubbleButton5.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Calendrier_avec_Date
        Me.BubbleButton5.Name = "BubbleButton5"
        Me.BubbleButton5.TooltipText = "Calendar"
        '
        'BubbleButton6
        '
        Me.BubbleButton6.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Calculator
        Me.BubbleButton6.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Calculator
        Me.BubbleButton6.Name = "BubbleButton6"
        Me.BubbleButton6.TooltipText = "Calculator"
        '
        'BubbleButton9
        '
        Me.BubbleButton9.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.NotePad
        Me.BubbleButton9.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.NotePad
        Me.BubbleButton9.Name = "BubbleButton9"
        Me.BubbleButton9.TooltipText = "Notes"
        '
        'BubbleButton10
        '
        Me.BubbleButton10.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Parametre
        Me.BubbleButton10.ImageLarge = Global.Feren_OS_DE_preview.My.Resources.Resources.Parametre
        Me.BubbleButton10.Name = "BubbleButton10"
        Me.BubbleButton10.TooltipText = "Settings"
        '
        'BubbleButton7
        '
        Me.BubbleButton7.Image = CType(resources.GetObject("BubbleButton7.Image"), System.Drawing.Image)
        Me.BubbleButton7.ImageLarge = CType(resources.GetObject("BubbleButton7.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton7.Name = "BubbleButton7"
        Me.BubbleButton7.TooltipText = "Power"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Deskmenu
        '
        Me.Deskmenu.BackColor = System.Drawing.Color.White
        Me.Deskmenu.Controls.Add(Me.Button7)
        Me.Deskmenu.Controls.Add(Me.Button6)
        Me.Deskmenu.Controls.Add(Me.Button5)
        Me.Deskmenu.Controls.Add(Me.Panel22)
        Me.Deskmenu.Controls.Add(Me.Panel20)
        Me.Deskmenu.Controls.Add(Me.Button1)
        Me.Deskmenu.Location = New System.Drawing.Point(20, 183)
        Me.Deskmenu.Name = "Deskmenu"
        Me.Deskmenu.Size = New System.Drawing.Size(189, 194)
        Me.Deskmenu.TabIndex = 25
        Me.Deskmenu.Visible = False
        '
        'Button1
        '
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(0, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(189, 38)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = " Change background..."
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.Panel21)
        Me.Panel20.Controls.Add(Me.Button2)
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel20.Location = New System.Drawing.Point(0, 38)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(189, 38)
        Me.Panel20.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(0, 0)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(137, 38)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = " Position"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.Button4)
        Me.Panel21.Controls.Add(Me.Button3)
        Me.Panel21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel21.Location = New System.Drawing.Point(137, 0)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(52, 38)
        Me.Panel21.TabIndex = 3
        '
        'Button3
        '
        Me.Button3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Button3.Location = New System.Drawing.Point(0, 19)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(52, 19)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Bottom"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button4.Location = New System.Drawing.Point(0, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(52, 19)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "Top"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Panel22
        '
        Me.Panel22.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel22.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel22.Location = New System.Drawing.Point(0, 76)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(189, 2)
        Me.Panel22.TabIndex = 3
        '
        'Button5
        '
        Me.Button5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Button5.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(0, 78)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(189, 38)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = " Lock Desk"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Button6.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(0, 116)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(189, 38)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = " Refresh"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Button7.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(0, 154)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(189, 38)
        Me.Button7.TabIndex = 6
        Me.Button7.Text = " Power..."
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Desktop
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gray
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(853, 579)
        Me.Controls.Add(Me.Deskmenu)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.BubbleBar1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel10)
        Me.Controls.Add(Me.Panel16)
        Me.Controls.Add(Me.Panel15)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Stepindicator1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.BatteryLevel)
        Me.Controls.Add(Me.DesktopBackImage)
        Me.Controls.Add(Me.Closable)
        Me.Controls.Add(Me.Panel18)
        Me.Controls.Add(Me.buildtag)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Desktop"
        Me.Text = "Desktop"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        CType(Me.TerminalIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookChangerIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SettingsIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NotesIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GalleryIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CalculatorIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CalendarIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StoreIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RiPlayerIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExplorerIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Explorermenu.ResumeLayout(False)
        CType(Me.KittenFoxIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.KittenFoxMenu.ResumeLayout(False)
        CType(Me.AppButton, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.WIFIIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Userbutton, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Userimagebutton, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.Panel2size, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.DesktopBackImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        CType(Me.BubbleBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Deskmenu.ResumeLayout(False)
        Me.Panel20.ResumeLayout(False)
        Me.Panel21.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents AppButton As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Userimagebutton As System.Windows.Forms.PictureBox
    Friend WithEvents buildtag As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Time As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents WIFIIcon As System.Windows.Forms.PictureBox
    Friend WithEvents Panel5 As System.Windows.Forms.MonthCalendar
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ZOSAssistantToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Closable As System.Windows.Forms.CheckBox
    Friend WithEvents DesktopBackImage As System.Windows.Forms.PictureBox
    Friend WithEvents BatteryLevel As System.Windows.Forms.ProgressBar
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Stepindicator1 As System.Windows.Forms.CheckBox
    Friend WithEvents StoreIcon As System.Windows.Forms.PictureBox
    Friend WithEvents RiPlayerIcon As System.Windows.Forms.PictureBox
    Friend WithEvents ExplorerIcon As System.Windows.Forms.PictureBox
    Friend WithEvents KittenFoxIcon As System.Windows.Forms.PictureBox
    Friend WithEvents CalendarIcon As System.Windows.Forms.PictureBox
    Friend WithEvents CalculatorIcon As System.Windows.Forms.PictureBox
    Friend WithEvents GalleryIcon As System.Windows.Forms.PictureBox
    Friend WithEvents SettingsIcon As System.Windows.Forms.PictureBox
    Friend WithEvents NotesIcon As System.Windows.Forms.PictureBox
    Friend WithEvents LookChangerIcon As System.Windows.Forms.PictureBox
    Friend WithEvents TerminalIcon As System.Windows.Forms.PictureBox
    Friend WithEvents KittenFoxMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MaximizeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MinimizeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Explorermenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents Loading As System.Windows.Forms.Timer
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel2size As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents RatingStar1 As DevComponents.DotNetBar.Controls.RatingStar
    Friend WithEvents MusicName As System.Windows.Forms.Label
    Friend WithEvents Userbutton As System.Windows.Forms.PictureBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents AutoProtection As System.Windows.Forms.CheckBox
    Friend WithEvents Wifi As System.Windows.Forms.CheckBox
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Slider1 As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents Slider2 As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents BubbleBar1 As DevComponents.DotNetBar.BubbleBar
    Friend WithEvents BubbleBarTab1 As DevComponents.DotNetBar.BubbleBarTab
    Friend WithEvents BubbleButton1 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton2 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton3 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton4 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton5 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton6 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton9 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton10 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton7 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents ContextMenuStrip3 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ChangeImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Deskmenu As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel22 As System.Windows.Forms.Panel
    Friend WithEvents Panel20 As System.Windows.Forms.Panel
    Friend WithEvents Panel21 As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
