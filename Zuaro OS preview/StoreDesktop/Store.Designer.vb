﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StoreDesktop
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StoreDesktop))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Closable = New System.Windows.Forms.CheckBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PictureBox36 = New System.Windows.Forms.PictureBox()
        Me.PictureBox37 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.FrontPage = New System.Windows.Forms.Panel()
        Me.NoConnection = New System.Windows.Forms.Panel()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.PictureBox38 = New System.Windows.Forms.PictureBox()
        Me.WebBrowsers = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.Antiviruses = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.PictureBox35 = New System.Windows.Forms.PictureBox()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.Button32 = New System.Windows.Forms.Button()
        Me.backbtn = New System.Windows.Forms.PictureBox()
        Me.everdaylife = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Button44 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.PictureBox34 = New System.Windows.Forms.PictureBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Button43 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Button41 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Button38 = New System.Windows.Forms.Button()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Button34 = New System.Windows.Forms.Button()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.PictureBox31 = New System.Windows.Forms.PictureBox()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button40 = New System.Windows.Forms.Button()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.PictureBox32 = New System.Windows.Forms.PictureBox()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.PictureBox33 = New System.Windows.Forms.PictureBox()
        Me.Button42 = New System.Windows.Forms.Button()
        Me.SkinnedTBar = New System.Windows.Forms.Panel()
        Me.skinbackbtn = New System.Windows.Forms.PictureBox()
        Me.PictureBox39 = New System.Windows.Forms.PictureBox()
        Me.PictureBox40 = New System.Windows.Forms.PictureBox()
        Me.PictureBox41 = New System.Windows.Forms.PictureBox()
        Me.PictureBox42 = New System.Windows.Forms.PictureBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.Button48 = New System.Windows.Forms.Button()
        Me.Button47 = New System.Windows.Forms.Button()
        Me.Button46 = New System.Windows.Forms.Button()
        Me.Button45 = New System.Windows.Forms.Button()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FrontPage.SuspendLayout()
        Me.NoConnection.SuspendLayout()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WebBrowsers.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Antiviruses.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel24.SuspendLayout()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel15.SuspendLayout()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel17.SuspendLayout()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel18.SuspendLayout()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.backbtn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.everdaylife.SuspendLayout()
        Me.Panel13.SuspendLayout()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel19.SuspendLayout()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel20.SuspendLayout()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel21.SuspendLayout()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel22.SuspendLayout()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel23.SuspendLayout()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SkinnedTBar.SuspendLayout()
        CType(Me.skinbackbtn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel26.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 250
        '
        'Closable
        '
        Me.Closable.AutoSize = True
        Me.Closable.Location = New System.Drawing.Point(-15, -16)
        Me.Closable.Name = "Closable"
        Me.Closable.Size = New System.Drawing.Size(81, 17)
        Me.Closable.TabIndex = 15
        Me.Closable.Text = "CheckBox1"
        Me.Closable.UseVisualStyleBackColor = True
        Me.Closable.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.BackgroundImage = CType(resources.GetObject("PictureBox3.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox3.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox3.Location = New System.Drawing.Point(794, 11)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(29, 56)
        Me.PictureBox3.TabIndex = 14
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.User_9__Carré_
        Me.PictureBox2.Location = New System.Drawing.Point(637, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(56, 55)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 11
        Me.PictureBox2.TabStop = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(-64, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(702, 55)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "User1"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Kozuka Gothic Pr6N R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label1.Location = New System.Drawing.Point(97, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 26)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "ZUARO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Kozuka Gothic Pro EL", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(92, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 48)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "store"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(12, 11)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(79, 84)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Location = New System.Drawing.Point(0, 496)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(825, 12)
        Me.Panel1.TabIndex = 16
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox4.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.Location = New System.Drawing.Point(324, -82)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(177, 176)
        Me.PictureBox4.TabIndex = 1
        Me.PictureBox4.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.BackgroundImage = CType(resources.GetObject("PictureBox5.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox5.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox5.Location = New System.Drawing.Point(729, 11)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(29, 56)
        Me.PictureBox5.TabIndex = 17
        Me.PictureBox5.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.BackgroundImage = CType(resources.GetObject("PictureBox6.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox6.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox6.Location = New System.Drawing.Point(762, 11)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(29, 56)
        Me.PictureBox6.TabIndex = 18
        Me.PictureBox6.TabStop = False
        '
        'PictureBox36
        '
        Me.PictureBox36.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox36.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox36.BackgroundImage = CType(resources.GetObject("PictureBox36.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox36.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox36.Location = New System.Drawing.Point(699, 11)
        Me.PictureBox36.Name = "PictureBox36"
        Me.PictureBox36.Size = New System.Drawing.Size(22, 56)
        Me.PictureBox36.TabIndex = 37
        Me.PictureBox36.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox36, "Change Skin...")
        '
        'PictureBox37
        '
        Me.PictureBox37.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox37.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox37.BackgroundImage = CType(resources.GetObject("PictureBox37.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox37.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox37.Location = New System.Drawing.Point(695, 8)
        Me.PictureBox37.Name = "PictureBox37"
        Me.PictureBox37.Size = New System.Drawing.Size(22, 56)
        Me.PictureBox37.TabIndex = 47
        Me.PictureBox37.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox37, "Change Skin...")
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox7.BackgroundImage = CType(resources.GetObject("PictureBox7.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox7.Location = New System.Drawing.Point(160, 44)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(82, 81)
        Me.PictureBox7.TabIndex = 19
        Me.PictureBox7.TabStop = False
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(138, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 22)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Internet Apps"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(341, 141)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 22)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Antiviruses"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox8.BackgroundImage = CType(resources.GetObject("PictureBox8.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox8.Location = New System.Drawing.Point(363, 44)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(82, 81)
        Me.PictureBox8.TabIndex = 21
        Me.PictureBox8.TabStop = False
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label6.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(558, 141)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 22)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Everyday Life"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox9.BackgroundImage = CType(resources.GetObject("PictureBox9.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox9.Location = New System.Drawing.Point(580, 44)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(82, 81)
        Me.PictureBox9.TabIndex = 23
        Me.PictureBox9.TabStop = False
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label7.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(138, 291)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 22)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Photo apps"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox10
        '
        Me.PictureBox10.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox10.BackgroundImage = CType(resources.GetObject("PictureBox10.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox10.Location = New System.Drawing.Point(160, 194)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(82, 81)
        Me.PictureBox10.TabIndex = 25
        Me.PictureBox10.TabStop = False
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label8.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(341, 291)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 22)
        Me.Label8.TabIndex = 28
        Me.Label8.Text = "Must haves"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox11
        '
        Me.PictureBox11.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox11.BackgroundImage = CType(resources.GetObject("PictureBox11.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox11.Location = New System.Drawing.Point(363, 194)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(82, 81)
        Me.PictureBox11.TabIndex = 27
        Me.PictureBox11.TabStop = False
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label9.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(558, 291)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(128, 22)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Search"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox12
        '
        Me.PictureBox12.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox12.BackgroundImage = CType(resources.GetObject("PictureBox12.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox12.Location = New System.Drawing.Point(580, 194)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(82, 81)
        Me.PictureBox12.TabIndex = 29
        Me.PictureBox12.TabStop = False
        '
        'FrontPage
        '
        Me.FrontPage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FrontPage.Controls.Add(Me.NoConnection)
        Me.FrontPage.Controls.Add(Me.PictureBox7)
        Me.FrontPage.Controls.Add(Me.Label9)
        Me.FrontPage.Controls.Add(Me.Label4)
        Me.FrontPage.Controls.Add(Me.PictureBox12)
        Me.FrontPage.Controls.Add(Me.PictureBox8)
        Me.FrontPage.Controls.Add(Me.Label8)
        Me.FrontPage.Controls.Add(Me.Label5)
        Me.FrontPage.Controls.Add(Me.PictureBox11)
        Me.FrontPage.Controls.Add(Me.PictureBox9)
        Me.FrontPage.Controls.Add(Me.Label7)
        Me.FrontPage.Controls.Add(Me.Label6)
        Me.FrontPage.Controls.Add(Me.PictureBox10)
        Me.FrontPage.Location = New System.Drawing.Point(0, 101)
        Me.FrontPage.Name = "FrontPage"
        Me.FrontPage.Size = New System.Drawing.Size(825, 407)
        Me.FrontPage.TabIndex = 31
        '
        'NoConnection
        '
        Me.NoConnection.BackColor = System.Drawing.Color.White
        Me.NoConnection.Controls.Add(Me.Label58)
        Me.NoConnection.Controls.Add(Me.Label59)
        Me.NoConnection.Controls.Add(Me.PictureBox38)
        Me.NoConnection.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NoConnection.Location = New System.Drawing.Point(0, 0)
        Me.NoConnection.Name = "NoConnection"
        Me.NoConnection.Size = New System.Drawing.Size(825, 407)
        Me.NoConnection.TabIndex = 31
        Me.NoConnection.Visible = False
        '
        'Label58
        '
        Me.Label58.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.Black
        Me.Label58.Location = New System.Drawing.Point(275, 202)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(451, 44)
        Me.Label58.TabIndex = 2
        Me.Label58.Text = "You cannot access the Store without an internet connection." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Please try again lat" & _
    "er..."
        '
        'Label59
        '
        Me.Label59.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.Color.Black
        Me.Label59.Location = New System.Drawing.Point(272, 160)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(352, 42)
        Me.Label59.TabIndex = 1
        Me.Label59.Text = "No internet connection"
        '
        'PictureBox38
        '
        Me.PictureBox38.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox38.BackgroundImage = CType(resources.GetObject("PictureBox38.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox38.Location = New System.Drawing.Point(104, 121)
        Me.PictureBox38.Name = "PictureBox38"
        Me.PictureBox38.Size = New System.Drawing.Size(161, 165)
        Me.PictureBox38.TabIndex = 0
        Me.PictureBox38.TabStop = False
        '
        'WebBrowsers
        '
        Me.WebBrowsers.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WebBrowsers.AutoScroll = True
        Me.WebBrowsers.Controls.Add(Me.Panel9)
        Me.WebBrowsers.Controls.Add(Me.Panel8)
        Me.WebBrowsers.Controls.Add(Me.Panel7)
        Me.WebBrowsers.Controls.Add(Me.Panel6)
        Me.WebBrowsers.Controls.Add(Me.Panel5)
        Me.WebBrowsers.Controls.Add(Me.Panel2)
        Me.WebBrowsers.Controls.Add(Me.Panel3)
        Me.WebBrowsers.Controls.Add(Me.Panel4)
        Me.WebBrowsers.Location = New System.Drawing.Point(0, 102)
        Me.WebBrowsers.Name = "WebBrowsers"
        Me.WebBrowsers.Size = New System.Drawing.Size(825, 406)
        Me.WebBrowsers.TabIndex = 32
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel9.Controls.Add(Me.Button15)
        Me.Panel9.Controls.Add(Me.Label24)
        Me.Panel9.Controls.Add(Me.Label25)
        Me.Panel9.Controls.Add(Me.PictureBox20)
        Me.Panel9.Controls.Add(Me.Button16)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel9.Location = New System.Drawing.Point(0, 504)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(808, 72)
        Me.Panel9.TabIndex = 7
        '
        'Button15
        '
        Me.Button15.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button15.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button15.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button15.FlatAppearance.BorderSize = 0
        Me.Button15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button15.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button15.ForeColor = System.Drawing.Color.White
        Me.Button15.Location = New System.Drawing.Point(612, 19)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(184, 35)
        Me.Button15.TabIndex = 26
        Me.Button15.Text = "Download and Install"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.DimGray
        Me.Label24.Location = New System.Drawing.Point(81, 42)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 20)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Comodo"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(79, 11)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(193, 32)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Comodo Dragon"
        '
        'PictureBox20
        '
        Me.PictureBox20.BackgroundImage = CType(resources.GetObject("PictureBox20.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox20.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox20.TabIndex = 0
        Me.PictureBox20.TabStop = False
        '
        'Button16
        '
        Me.Button16.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button16.BackColor = System.Drawing.Color.DimGray
        Me.Button16.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button16.FlatAppearance.BorderSize = 0
        Me.Button16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button16.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button16.ForeColor = System.Drawing.Color.White
        Me.Button16.Location = New System.Drawing.Point(612, 19)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(184, 35)
        Me.Button16.TabIndex = 28
        Me.Button16.Text = "Installed"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel8.Controls.Add(Me.Button13)
        Me.Panel8.Controls.Add(Me.Label22)
        Me.Panel8.Controls.Add(Me.Label23)
        Me.Panel8.Controls.Add(Me.PictureBox19)
        Me.Panel8.Controls.Add(Me.Button14)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(0, 432)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(808, 72)
        Me.Panel8.TabIndex = 6
        '
        'Button13
        '
        Me.Button13.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button13.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button13.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button13.FlatAppearance.BorderSize = 0
        Me.Button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.White
        Me.Button13.Location = New System.Drawing.Point(612, 19)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(184, 35)
        Me.Button13.TabIndex = 26
        Me.Button13.Text = "Download and Install"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.DimGray
        Me.Label22.Location = New System.Drawing.Point(81, 42)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(16, 20)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "?"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(79, 11)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(110, 32)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Maxthon"
        '
        'PictureBox19
        '
        Me.PictureBox19.BackgroundImage = CType(resources.GetObject("PictureBox19.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox19.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox19.TabIndex = 0
        Me.PictureBox19.TabStop = False
        '
        'Button14
        '
        Me.Button14.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button14.BackColor = System.Drawing.Color.DimGray
        Me.Button14.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button14.FlatAppearance.BorderSize = 0
        Me.Button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.White
        Me.Button14.Location = New System.Drawing.Point(612, 19)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(184, 35)
        Me.Button14.TabIndex = 28
        Me.Button14.Text = "Installed"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel7.Controls.Add(Me.Button11)
        Me.Panel7.Controls.Add(Me.Label20)
        Me.Panel7.Controls.Add(Me.Label21)
        Me.Panel7.Controls.Add(Me.PictureBox18)
        Me.Panel7.Controls.Add(Me.Button12)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(0, 360)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(808, 72)
        Me.Panel7.TabIndex = 5
        '
        'Button11
        '
        Me.Button11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button11.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button11.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button11.FlatAppearance.BorderSize = 0
        Me.Button11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.White
        Me.Button11.Location = New System.Drawing.Point(612, 19)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(184, 35)
        Me.Button11.TabIndex = 26
        Me.Button11.Text = "Download and Install"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DimGray
        Me.Label20.Location = New System.Drawing.Point(81, 42)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(16, 20)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "?"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(79, 11)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(115, 32)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Rockmelt"
        '
        'PictureBox18
        '
        Me.PictureBox18.BackgroundImage = CType(resources.GetObject("PictureBox18.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox18.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox18.TabIndex = 0
        Me.PictureBox18.TabStop = False
        '
        'Button12
        '
        Me.Button12.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button12.BackColor = System.Drawing.Color.DimGray
        Me.Button12.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button12.FlatAppearance.BorderSize = 0
        Me.Button12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(612, 19)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(184, 35)
        Me.Button12.TabIndex = 28
        Me.Button12.Text = "Installed"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel6.Controls.Add(Me.Button9)
        Me.Panel6.Controls.Add(Me.Label18)
        Me.Panel6.Controls.Add(Me.Label19)
        Me.Panel6.Controls.Add(Me.PictureBox17)
        Me.Panel6.Controls.Add(Me.Button10)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(0, 288)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(808, 72)
        Me.Panel6.TabIndex = 4
        '
        'Button9
        '
        Me.Button9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button9.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button9.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button9.FlatAppearance.BorderSize = 0
        Me.Button9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(612, 19)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(184, 35)
        Me.Button9.TabIndex = 26
        Me.Button9.Text = "Download and Install"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.DimGray
        Me.Label18.Location = New System.Drawing.Point(81, 42)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 20)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "Opera"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(79, 11)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(81, 32)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Opera"
        '
        'PictureBox17
        '
        Me.PictureBox17.BackgroundImage = CType(resources.GetObject("PictureBox17.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox17.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox17.TabIndex = 0
        Me.PictureBox17.TabStop = False
        '
        'Button10
        '
        Me.Button10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button10.BackColor = System.Drawing.Color.DimGray
        Me.Button10.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button10.FlatAppearance.BorderSize = 0
        Me.Button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.Location = New System.Drawing.Point(612, 19)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(184, 35)
        Me.Button10.TabIndex = 28
        Me.Button10.Text = "Installed"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel5.Controls.Add(Me.Button7)
        Me.Panel5.Controls.Add(Me.Label16)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.PictureBox16)
        Me.Panel5.Controls.Add(Me.Button8)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 216)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(808, 72)
        Me.Panel5.TabIndex = 3
        '
        'Button7
        '
        Me.Button7.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button7.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button7.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button7.FlatAppearance.BorderSize = 0
        Me.Button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(612, 19)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(184, 35)
        Me.Button7.TabIndex = 26
        Me.Button7.Text = "Download and Install"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.DimGray
        Me.Label16.Location = New System.Drawing.Point(81, 42)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(49, 20)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Apple"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(79, 11)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(220, 32)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Safari Web Browser"
        '
        'PictureBox16
        '
        Me.PictureBox16.BackgroundImage = CType(resources.GetObject("PictureBox16.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox16.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox16.TabIndex = 0
        Me.PictureBox16.TabStop = False
        '
        'Button8
        '
        Me.Button8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button8.BackColor = System.Drawing.Color.DimGray
        Me.Button8.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button8.FlatAppearance.BorderSize = 0
        Me.Button8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(612, 19)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(184, 35)
        Me.Button8.TabIndex = 28
        Me.Button8.Text = "Installed"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.Button5)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.PictureBox15)
        Me.Panel2.Controls.Add(Me.Button6)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 144)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(808, 72)
        Me.Panel2.TabIndex = 2
        '
        'Button5
        '
        Me.Button5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button5.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button5.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(612, 19)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(184, 35)
        Me.Button5.TabIndex = 26
        Me.Button5.Text = "Download and Install"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.DimGray
        Me.Label14.Location = New System.Drawing.Point(81, 42)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 20)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Mozilla"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(79, 11)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(169, 32)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Mozilla Firefox"
        '
        'PictureBox15
        '
        Me.PictureBox15.BackgroundImage = CType(resources.GetObject("PictureBox15.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox15.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox15.TabIndex = 0
        Me.PictureBox15.TabStop = False
        '
        'Button6
        '
        Me.Button6.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button6.BackColor = System.Drawing.Color.DimGray
        Me.Button6.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button6.FlatAppearance.BorderSize = 0
        Me.Button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(612, 19)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(184, 35)
        Me.Button6.TabIndex = 28
        Me.Button6.Text = "Installed"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.PictureBox14)
        Me.Panel3.Controls.Add(Me.Button4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 72)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(808, 72)
        Me.Panel3.TabIndex = 1
        '
        'Button2
        '
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button2.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(612, 19)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(184, 35)
        Me.Button2.TabIndex = 26
        Me.Button2.Text = "Download and Install"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.DimGray
        Me.Label12.Location = New System.Drawing.Point(81, 42)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 20)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Microsoft"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(79, 11)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(437, 32)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Internet Explorer (Included in Windows)"
        '
        'PictureBox14
        '
        Me.PictureBox14.BackgroundImage = CType(resources.GetObject("PictureBox14.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox14.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox14.TabIndex = 0
        Me.PictureBox14.TabStop = False
        '
        'Button4
        '
        Me.Button4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button4.BackColor = System.Drawing.Color.DimGray
        Me.Button4.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(612, 19)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(184, 35)
        Me.Button4.TabIndex = 28
        Me.Button4.Text = "Installed"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Button1)
        Me.Panel4.Controls.Add(Me.Label11)
        Me.Panel4.Controls.Add(Me.Label10)
        Me.Panel4.Controls.Add(Me.PictureBox13)
        Me.Panel4.Controls.Add(Me.Button3)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(808, 72)
        Me.Panel4.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(612, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(184, 35)
        Me.Button1.TabIndex = 26
        Me.Button1.Text = "Download and Install"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.DimGray
        Me.Label11.Location = New System.Drawing.Point(81, 42)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 20)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Google"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(79, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(185, 32)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Google Chrome"
        '
        'PictureBox13
        '
        Me.PictureBox13.BackgroundImage = CType(resources.GetObject("PictureBox13.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox13.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox13.TabIndex = 0
        Me.PictureBox13.TabStop = False
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button3.BackColor = System.Drawing.Color.DimGray
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(612, 19)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(184, 35)
        Me.Button3.TabIndex = 27
        Me.Button3.Text = "Installed"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(794, 73)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.TabIndex = 33
        Me.WebBrowser1.Visible = False
        '
        'Antiviruses
        '
        Me.Antiviruses.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Antiviruses.AutoScroll = True
        Me.Antiviruses.Controls.Add(Me.Panel14)
        Me.Antiviruses.Controls.Add(Me.Panel15)
        Me.Antiviruses.Controls.Add(Me.Panel16)
        Me.Antiviruses.Controls.Add(Me.Panel17)
        Me.Antiviruses.Controls.Add(Me.Panel18)
        Me.Antiviruses.Location = New System.Drawing.Point(0, 102)
        Me.Antiviruses.Name = "Antiviruses"
        Me.Antiviruses.Size = New System.Drawing.Size(825, 406)
        Me.Antiviruses.TabIndex = 34
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel14.Controls.Add(Me.Panel24)
        Me.Panel14.Controls.Add(Me.Button21)
        Me.Panel14.Controls.Add(Me.Button22)
        Me.Panel14.Controls.Add(Me.Label32)
        Me.Panel14.Controls.Add(Me.Label33)
        Me.Panel14.Controls.Add(Me.PictureBox24)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel14.Location = New System.Drawing.Point(0, 288)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(825, 100)
        Me.Panel14.TabIndex = 4
        '
        'Panel24
        '
        Me.Panel24.BackColor = System.Drawing.Color.Honeydew
        Me.Panel24.Controls.Add(Me.Label54)
        Me.Panel24.Controls.Add(Me.PictureBox35)
        Me.Panel24.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel24.Location = New System.Drawing.Point(0, 70)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(825, 30)
        Me.Panel24.TabIndex = 32
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.Black
        Me.Label54.Location = New System.Drawing.Point(74, 5)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(479, 21)
        Me.Label54.TabIndex = 1
        Me.Label54.Text = "Protects against CryptoLocker (Crilock) File encrypting ransomware."
        '
        'PictureBox35
        '
        Me.PictureBox35.BackgroundImage = CType(resources.GetObject("PictureBox35.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox35.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox35.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox35.Name = "PictureBox35"
        Me.PictureBox35.Size = New System.Drawing.Size(91, 30)
        Me.PictureBox35.TabIndex = 0
        Me.PictureBox35.TabStop = False
        '
        'Button21
        '
        Me.Button21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button21.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button21.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button21.FlatAppearance.BorderSize = 0
        Me.Button21.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button21.ForeColor = System.Drawing.Color.White
        Me.Button21.Location = New System.Drawing.Point(563, 19)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(117, 35)
        Me.Button21.TabIndex = 31
        Me.Button21.Text = "Buy online"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'Button22
        '
        Me.Button22.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button22.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button22.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button22.FlatAppearance.BorderSize = 0
        Me.Button22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button22.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button22.ForeColor = System.Drawing.Color.White
        Me.Button22.Location = New System.Drawing.Point(686, 19)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(127, 35)
        Me.Button22.TabIndex = 30
        Me.Button22.Text = "Get free trial"
        Me.Button22.UseVisualStyleBackColor = False
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.DimGray
        Me.Label32.Location = New System.Drawing.Point(81, 42)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(75, 20)
        Me.Label32.TabIndex = 2
        Me.Label32.Text = "Bullguard"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.Black
        Me.Label33.Location = New System.Drawing.Point(79, 11)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(298, 32)
        Me.Label33.TabIndex = 1
        Me.Label33.Text = "Bullguard Internet Security"
        '
        'PictureBox24
        '
        Me.PictureBox24.BackgroundImage = CType(resources.GetObject("PictureBox24.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox24.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox24.TabIndex = 0
        Me.PictureBox24.TabStop = False
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel15.Controls.Add(Me.Button25)
        Me.Panel15.Controls.Add(Me.Label34)
        Me.Panel15.Controls.Add(Me.Label35)
        Me.Panel15.Controls.Add(Me.PictureBox25)
        Me.Panel15.Controls.Add(Me.Button26)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel15.Location = New System.Drawing.Point(0, 216)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(825, 72)
        Me.Panel15.TabIndex = 3
        '
        'Button25
        '
        Me.Button25.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button25.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button25.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button25.FlatAppearance.BorderSize = 0
        Me.Button25.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button25.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button25.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button25.ForeColor = System.Drawing.Color.White
        Me.Button25.Location = New System.Drawing.Point(629, 19)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(184, 35)
        Me.Button25.TabIndex = 26
        Me.Button25.Text = "Download and Install"
        Me.Button25.UseVisualStyleBackColor = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.DimGray
        Me.Label34.Location = New System.Drawing.Point(81, 42)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(44, 20)
        Me.Label34.TabIndex = 2
        Me.Label34.Text = "Avira"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.Black
        Me.Label35.Location = New System.Drawing.Point(79, 11)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(217, 32)
        Me.Label35.TabIndex = 1
        Me.Label35.Text = "Avira Free antivirus"
        '
        'PictureBox25
        '
        Me.PictureBox25.BackColor = System.Drawing.Color.Red
        Me.PictureBox25.BackgroundImage = CType(resources.GetObject("PictureBox25.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox25.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox25.TabIndex = 0
        Me.PictureBox25.TabStop = False
        '
        'Button26
        '
        Me.Button26.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button26.BackColor = System.Drawing.Color.DimGray
        Me.Button26.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button26.FlatAppearance.BorderSize = 0
        Me.Button26.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button26.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button26.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button26.ForeColor = System.Drawing.Color.White
        Me.Button26.Location = New System.Drawing.Point(629, 19)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(184, 35)
        Me.Button26.TabIndex = 28
        Me.Button26.Text = "Installed"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'Panel16
        '
        Me.Panel16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel16.Controls.Add(Me.Label36)
        Me.Panel16.Controls.Add(Me.Label37)
        Me.Panel16.Controls.Add(Me.PictureBox26)
        Me.Panel16.Controls.Add(Me.Button28)
        Me.Panel16.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel16.Location = New System.Drawing.Point(0, 144)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(825, 72)
        Me.Panel16.TabIndex = 2
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.DimGray
        Me.Label36.Location = New System.Drawing.Point(81, 42)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(49, 20)
        Me.Label36.TabIndex = 2
        Me.Label36.Text = "Zuaro"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.Black
        Me.Label37.Location = New System.Drawing.Point(79, 11)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(114, 32)
        Me.Label37.TabIndex = 1
        Me.Label37.Text = "Defender"
        '
        'PictureBox26
        '
        Me.PictureBox26.BackgroundImage = CType(resources.GetObject("PictureBox26.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox26.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox26.TabIndex = 0
        Me.PictureBox26.TabStop = False
        '
        'Button28
        '
        Me.Button28.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button28.BackColor = System.Drawing.Color.DimGray
        Me.Button28.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button28.FlatAppearance.BorderSize = 0
        Me.Button28.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button28.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button28.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button28.ForeColor = System.Drawing.Color.White
        Me.Button28.Location = New System.Drawing.Point(597, 19)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(216, 35)
        Me.Button28.TabIndex = 28
        Me.Button28.Text = "Pre-installed with Zuaro OS"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel17.Controls.Add(Me.Button33)
        Me.Panel17.Controls.Add(Me.Button29)
        Me.Panel17.Controls.Add(Me.Label38)
        Me.Panel17.Controls.Add(Me.Label39)
        Me.Panel17.Controls.Add(Me.PictureBox27)
        Me.Panel17.Controls.Add(Me.Button30)
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel17.Location = New System.Drawing.Point(0, 72)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(825, 72)
        Me.Panel17.TabIndex = 1
        '
        'Button33
        '
        Me.Button33.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button33.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button33.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button33.FlatAppearance.BorderSize = 0
        Me.Button33.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button33.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button33.ForeColor = System.Drawing.Color.White
        Me.Button33.Location = New System.Drawing.Point(563, 19)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(117, 35)
        Me.Button33.TabIndex = 29
        Me.Button33.Text = "Buy online"
        Me.Button33.UseVisualStyleBackColor = False
        '
        'Button29
        '
        Me.Button29.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button29.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button29.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button29.FlatAppearance.BorderSize = 0
        Me.Button29.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button29.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button29.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button29.ForeColor = System.Drawing.Color.White
        Me.Button29.Location = New System.Drawing.Point(686, 19)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(127, 35)
        Me.Button29.TabIndex = 26
        Me.Button29.Text = "Get free trial"
        Me.Button29.UseVisualStyleBackColor = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.DimGray
        Me.Label38.Location = New System.Drawing.Point(81, 42)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(57, 20)
        Me.Label38.TabIndex = 2
        Me.Label38.Text = "Norton"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.Color.Black
        Me.Label39.Location = New System.Drawing.Point(79, 11)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(413, 32)
        Me.Label39.TabIndex = 1
        Me.Label39.Text = "Norton 360 (Award winning antivirus)"
        '
        'PictureBox27
        '
        Me.PictureBox27.BackgroundImage = CType(resources.GetObject("PictureBox27.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox27.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox27.TabIndex = 0
        Me.PictureBox27.TabStop = False
        '
        'Button30
        '
        Me.Button30.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button30.BackColor = System.Drawing.Color.DimGray
        Me.Button30.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button30.FlatAppearance.BorderSize = 0
        Me.Button30.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button30.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button30.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button30.ForeColor = System.Drawing.Color.White
        Me.Button30.Location = New System.Drawing.Point(686, 19)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(127, 35)
        Me.Button30.TabIndex = 28
        Me.Button30.Text = "Installed"
        Me.Button30.UseVisualStyleBackColor = False
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel18.Controls.Add(Me.Button31)
        Me.Panel18.Controls.Add(Me.Label40)
        Me.Panel18.Controls.Add(Me.Label41)
        Me.Panel18.Controls.Add(Me.PictureBox28)
        Me.Panel18.Controls.Add(Me.Button32)
        Me.Panel18.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel18.Location = New System.Drawing.Point(0, 0)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(825, 72)
        Me.Panel18.TabIndex = 0
        '
        'Button31
        '
        Me.Button31.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button31.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button31.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button31.FlatAppearance.BorderSize = 0
        Me.Button31.FlatAppearance.MouseDownBackColor = System.Drawing.Color.YellowGreen
        Me.Button31.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LawnGreen
        Me.Button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button31.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button31.ForeColor = System.Drawing.Color.White
        Me.Button31.Location = New System.Drawing.Point(629, 19)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(184, 35)
        Me.Button31.TabIndex = 26
        Me.Button31.Text = "Download and Install"
        Me.Button31.UseVisualStyleBackColor = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.DimGray
        Me.Label40.Location = New System.Drawing.Point(81, 42)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(50, 20)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "Avast!"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.Black
        Me.Label41.Location = New System.Drawing.Point(79, 11)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(175, 32)
        Me.Label41.TabIndex = 1
        Me.Label41.Text = "Avast! antivirus"
        '
        'PictureBox28
        '
        Me.PictureBox28.BackgroundImage = CType(resources.GetObject("PictureBox28.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox28.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox28.TabIndex = 0
        Me.PictureBox28.TabStop = False
        '
        'Button32
        '
        Me.Button32.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button32.BackColor = System.Drawing.Color.DimGray
        Me.Button32.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button32.FlatAppearance.BorderSize = 0
        Me.Button32.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button32.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button32.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button32.ForeColor = System.Drawing.Color.White
        Me.Button32.Location = New System.Drawing.Point(629, 19)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(184, 35)
        Me.Button32.TabIndex = 27
        Me.Button32.Text = "Installed"
        Me.Button32.UseVisualStyleBackColor = False
        '
        'backbtn
        '
        Me.backbtn.BackgroundImage = CType(resources.GetObject("backbtn.BackgroundImage"), System.Drawing.Image)
        Me.backbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.backbtn.Location = New System.Drawing.Point(189, 21)
        Me.backbtn.Name = "backbtn"
        Me.backbtn.Size = New System.Drawing.Size(65, 63)
        Me.backbtn.TabIndex = 35
        Me.backbtn.TabStop = False
        Me.backbtn.Visible = False
        '
        'everdaylife
        '
        Me.everdaylife.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.everdaylife.AutoScroll = True
        Me.everdaylife.Controls.Add(Me.Panel13)
        Me.everdaylife.Controls.Add(Me.Panel12)
        Me.everdaylife.Controls.Add(Me.Panel11)
        Me.everdaylife.Controls.Add(Me.Panel10)
        Me.everdaylife.Controls.Add(Me.Panel19)
        Me.everdaylife.Controls.Add(Me.Panel20)
        Me.everdaylife.Controls.Add(Me.Panel21)
        Me.everdaylife.Controls.Add(Me.Panel22)
        Me.everdaylife.Controls.Add(Me.Panel23)
        Me.everdaylife.Location = New System.Drawing.Point(0, 101)
        Me.everdaylife.Name = "everdaylife"
        Me.everdaylife.Size = New System.Drawing.Size(825, 407)
        Me.everdaylife.TabIndex = 36
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel13.Controls.Add(Me.Button44)
        Me.Panel13.Controls.Add(Me.Button20)
        Me.Panel13.Controls.Add(Me.Label52)
        Me.Panel13.Controls.Add(Me.Label53)
        Me.Panel13.Controls.Add(Me.PictureBox34)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel13.Location = New System.Drawing.Point(0, 576)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(808, 72)
        Me.Panel13.TabIndex = 8
        '
        'Button44
        '
        Me.Button44.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button44.BackColor = System.Drawing.Color.YellowGreen
        Me.Button44.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button44.FlatAppearance.BorderSize = 0
        Me.Button44.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button44.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button44.ForeColor = System.Drawing.Color.White
        Me.Button44.Location = New System.Drawing.Point(629, 19)
        Me.Button44.Name = "Button44"
        Me.Button44.Size = New System.Drawing.Size(167, 35)
        Me.Button44.TabIndex = 30
        Me.Button44.Text = "Install"
        Me.Button44.UseVisualStyleBackColor = False
        '
        'Button20
        '
        Me.Button20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button20.BackColor = System.Drawing.Color.DimGray
        Me.Button20.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button20.FlatAppearance.BorderSize = 0
        Me.Button20.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button20.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button20.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button20.ForeColor = System.Drawing.Color.White
        Me.Button20.Location = New System.Drawing.Point(629, 19)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(167, 35)
        Me.Button20.TabIndex = 29
        Me.Button20.Text = "Installed"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label52.ForeColor = System.Drawing.Color.DimGray
        Me.Label52.Location = New System.Drawing.Point(81, 42)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(63, 20)
        Me.Label52.TabIndex = 2
        Me.Label52.Text = "linkedin"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.ForeColor = System.Drawing.Color.Black
        Me.Label53.Location = New System.Drawing.Point(79, 11)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(105, 32)
        Me.Label53.TabIndex = 1
        Me.Label53.Text = "Linkedin"
        '
        'PictureBox34
        '
        Me.PictureBox34.BackgroundImage = CType(resources.GetObject("PictureBox34.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox34.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox34.Name = "PictureBox34"
        Me.PictureBox34.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox34.TabIndex = 0
        Me.PictureBox34.TabStop = False
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel12.Controls.Add(Me.Button43)
        Me.Panel12.Controls.Add(Me.Button19)
        Me.Panel12.Controls.Add(Me.Label30)
        Me.Panel12.Controls.Add(Me.Label31)
        Me.Panel12.Controls.Add(Me.PictureBox23)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel12.Location = New System.Drawing.Point(0, 504)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(808, 72)
        Me.Panel12.TabIndex = 7
        '
        'Button43
        '
        Me.Button43.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button43.BackColor = System.Drawing.Color.YellowGreen
        Me.Button43.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button43.FlatAppearance.BorderSize = 0
        Me.Button43.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button43.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button43.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button43.ForeColor = System.Drawing.Color.White
        Me.Button43.Location = New System.Drawing.Point(629, 19)
        Me.Button43.Name = "Button43"
        Me.Button43.Size = New System.Drawing.Size(167, 35)
        Me.Button43.TabIndex = 30
        Me.Button43.Text = "Install"
        Me.Button43.UseVisualStyleBackColor = False
        '
        'Button19
        '
        Me.Button19.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button19.BackColor = System.Drawing.Color.DimGray
        Me.Button19.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button19.FlatAppearance.BorderSize = 0
        Me.Button19.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button19.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button19.ForeColor = System.Drawing.Color.White
        Me.Button19.Location = New System.Drawing.Point(629, 19)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(167, 35)
        Me.Button19.TabIndex = 29
        Me.Button19.Text = "Installed"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.DimGray
        Me.Label30.Location = New System.Drawing.Point(81, 42)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(55, 20)
        Me.Label30.TabIndex = 2
        Me.Label30.Text = "tumblr"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Black
        Me.Label31.Location = New System.Drawing.Point(79, 11)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(93, 32)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Tumblr"
        '
        'PictureBox23
        '
        Me.PictureBox23.BackgroundImage = CType(resources.GetObject("PictureBox23.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox23.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox23.TabIndex = 0
        Me.PictureBox23.TabStop = False
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel11.Controls.Add(Me.Button41)
        Me.Panel11.Controls.Add(Me.Button18)
        Me.Panel11.Controls.Add(Me.Label28)
        Me.Panel11.Controls.Add(Me.Label29)
        Me.Panel11.Controls.Add(Me.PictureBox22)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel11.Location = New System.Drawing.Point(0, 432)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(808, 72)
        Me.Panel11.TabIndex = 6
        '
        'Button41
        '
        Me.Button41.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button41.BackColor = System.Drawing.Color.YellowGreen
        Me.Button41.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button41.FlatAppearance.BorderSize = 0
        Me.Button41.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button41.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button41.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button41.ForeColor = System.Drawing.Color.White
        Me.Button41.Location = New System.Drawing.Point(629, 19)
        Me.Button41.Name = "Button41"
        Me.Button41.Size = New System.Drawing.Size(167, 35)
        Me.Button41.TabIndex = 31
        Me.Button41.Text = "Install"
        Me.Button41.UseVisualStyleBackColor = False
        '
        'Button18
        '
        Me.Button18.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button18.BackColor = System.Drawing.Color.DimGray
        Me.Button18.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button18.FlatAppearance.BorderSize = 0
        Me.Button18.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button18.ForeColor = System.Drawing.Color.White
        Me.Button18.Location = New System.Drawing.Point(629, 19)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(167, 35)
        Me.Button18.TabIndex = 29
        Me.Button18.Text = "Installed"
        Me.Button18.UseVisualStyleBackColor = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.DimGray
        Me.Label28.Location = New System.Drawing.Point(81, 42)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(72, 20)
        Me.Label28.TabIndex = 2
        Me.Label28.Text = "Microsoft"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.Black
        Me.Label29.Location = New System.Drawing.Point(79, 11)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(65, 32)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "MSN"
        '
        'PictureBox22
        '
        Me.PictureBox22.BackgroundImage = CType(resources.GetObject("PictureBox22.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox22.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox22.TabIndex = 0
        Me.PictureBox22.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel10.Controls.Add(Me.Button39)
        Me.Panel10.Controls.Add(Me.Button17)
        Me.Panel10.Controls.Add(Me.Label26)
        Me.Panel10.Controls.Add(Me.Label27)
        Me.Panel10.Controls.Add(Me.PictureBox21)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel10.Location = New System.Drawing.Point(0, 360)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(808, 72)
        Me.Panel10.TabIndex = 5
        '
        'Button39
        '
        Me.Button39.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button39.BackColor = System.Drawing.Color.YellowGreen
        Me.Button39.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button39.FlatAppearance.BorderSize = 0
        Me.Button39.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button39.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button39.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button39.ForeColor = System.Drawing.Color.White
        Me.Button39.Location = New System.Drawing.Point(629, 19)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(167, 35)
        Me.Button39.TabIndex = 30
        Me.Button39.Text = "Install"
        Me.Button39.UseVisualStyleBackColor = False
        '
        'Button17
        '
        Me.Button17.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button17.BackColor = System.Drawing.Color.DimGray
        Me.Button17.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button17.FlatAppearance.BorderSize = 0
        Me.Button17.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button17.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button17.ForeColor = System.Drawing.Color.White
        Me.Button17.Location = New System.Drawing.Point(629, 19)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(167, 35)
        Me.Button17.TabIndex = 29
        Me.Button17.Text = "Installed"
        Me.Button17.UseVisualStyleBackColor = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.DimGray
        Me.Label26.Location = New System.Drawing.Point(81, 42)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(42, 20)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "Foxit"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(79, 11)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(189, 32)
        Me.Label27.TabIndex = 1
        Me.Label27.Text = "Foxit PDF reader"
        '
        'PictureBox21
        '
        Me.PictureBox21.BackgroundImage = CType(resources.GetObject("PictureBox21.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox21.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox21.TabIndex = 0
        Me.PictureBox21.TabStop = False
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel19.Controls.Add(Me.Button38)
        Me.Panel19.Controls.Add(Me.Button36)
        Me.Panel19.Controls.Add(Me.Label42)
        Me.Panel19.Controls.Add(Me.Label43)
        Me.Panel19.Controls.Add(Me.PictureBox29)
        Me.Panel19.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel19.Location = New System.Drawing.Point(0, 288)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(808, 72)
        Me.Panel19.TabIndex = 4
        '
        'Button38
        '
        Me.Button38.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button38.BackColor = System.Drawing.Color.YellowGreen
        Me.Button38.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button38.FlatAppearance.BorderSize = 0
        Me.Button38.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button38.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button38.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button38.ForeColor = System.Drawing.Color.White
        Me.Button38.Location = New System.Drawing.Point(629, 19)
        Me.Button38.Name = "Button38"
        Me.Button38.Size = New System.Drawing.Size(167, 35)
        Me.Button38.TabIndex = 30
        Me.Button38.Text = "Install"
        Me.Button38.UseVisualStyleBackColor = False
        '
        'Button36
        '
        Me.Button36.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button36.BackColor = System.Drawing.Color.DimGray
        Me.Button36.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button36.FlatAppearance.BorderSize = 0
        Me.Button36.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button36.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button36.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button36.ForeColor = System.Drawing.Color.White
        Me.Button36.Location = New System.Drawing.Point(629, 19)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(167, 35)
        Me.Button36.TabIndex = 29
        Me.Button36.Text = "Installed"
        Me.Button36.UseVisualStyleBackColor = False
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.DimGray
        Me.Label42.Location = New System.Drawing.Point(81, 42)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(43, 20)
        Me.Label42.TabIndex = 2
        Me.Label42.Text = "Libre"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.Black
        Me.Label43.Location = New System.Drawing.Point(79, 11)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(324, 32)
        Me.Label43.TabIndex = 1
        Me.Label43.Text = "Libre Office (Full Office suite)"
        '
        'PictureBox29
        '
        Me.PictureBox29.BackgroundImage = CType(resources.GetObject("PictureBox29.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox29.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox29.TabIndex = 0
        Me.PictureBox29.TabStop = False
        '
        'Panel20
        '
        Me.Panel20.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel20.Controls.Add(Me.Button34)
        Me.Panel20.Controls.Add(Me.Button35)
        Me.Panel20.Controls.Add(Me.Label44)
        Me.Panel20.Controls.Add(Me.Label45)
        Me.Panel20.Controls.Add(Me.PictureBox30)
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel20.Location = New System.Drawing.Point(0, 216)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(808, 72)
        Me.Panel20.TabIndex = 3
        '
        'Button34
        '
        Me.Button34.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button34.BackColor = System.Drawing.Color.YellowGreen
        Me.Button34.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button34.FlatAppearance.BorderSize = 0
        Me.Button34.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button34.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button34.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button34.ForeColor = System.Drawing.Color.White
        Me.Button34.Location = New System.Drawing.Point(629, 19)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(167, 35)
        Me.Button34.TabIndex = 29
        Me.Button34.Text = "Install"
        Me.Button34.UseVisualStyleBackColor = False
        '
        'Button35
        '
        Me.Button35.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button35.BackColor = System.Drawing.Color.DimGray
        Me.Button35.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button35.FlatAppearance.BorderSize = 0
        Me.Button35.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button35.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button35.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button35.ForeColor = System.Drawing.Color.White
        Me.Button35.Location = New System.Drawing.Point(629, 19)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(167, 35)
        Me.Button35.TabIndex = 28
        Me.Button35.Text = "Installed"
        Me.Button35.UseVisualStyleBackColor = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.DimGray
        Me.Label44.Location = New System.Drawing.Point(81, 42)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(72, 20)
        Me.Label44.TabIndex = 2
        Me.Label44.Text = "Microsoft"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.Black
        Me.Label45.Location = New System.Drawing.Point(79, 11)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(140, 32)
        Me.Label45.TabIndex = 1
        Me.Label45.Text = "Bing Search"
        '
        'PictureBox30
        '
        Me.PictureBox30.BackgroundImage = CType(resources.GetObject("PictureBox30.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox30.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox30.TabIndex = 0
        Me.PictureBox30.TabStop = False
        '
        'Panel21
        '
        Me.Panel21.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel21.Controls.Add(Me.Button27)
        Me.Panel21.Controls.Add(Me.Button37)
        Me.Panel21.Controls.Add(Me.Label46)
        Me.Panel21.Controls.Add(Me.Label47)
        Me.Panel21.Controls.Add(Me.PictureBox31)
        Me.Panel21.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel21.Location = New System.Drawing.Point(0, 144)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(808, 72)
        Me.Panel21.TabIndex = 2
        '
        'Button27
        '
        Me.Button27.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button27.BackColor = System.Drawing.Color.YellowGreen
        Me.Button27.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button27.FlatAppearance.BorderSize = 0
        Me.Button27.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button27.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button27.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button27.ForeColor = System.Drawing.Color.White
        Me.Button27.Location = New System.Drawing.Point(629, 19)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(167, 35)
        Me.Button27.TabIndex = 29
        Me.Button27.Text = "Install"
        Me.Button27.UseVisualStyleBackColor = False
        '
        'Button37
        '
        Me.Button37.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button37.BackColor = System.Drawing.Color.DimGray
        Me.Button37.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button37.FlatAppearance.BorderSize = 0
        Me.Button37.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button37.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button37.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button37.ForeColor = System.Drawing.Color.White
        Me.Button37.Location = New System.Drawing.Point(629, 19)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(167, 35)
        Me.Button37.TabIndex = 28
        Me.Button37.Text = "Installed"
        Me.Button37.UseVisualStyleBackColor = False
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.DimGray
        Me.Label46.Location = New System.Drawing.Point(81, 42)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(58, 20)
        Me.Label46.TabIndex = 2
        Me.Label46.Text = "Google"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.Black
        Me.Label47.Location = New System.Drawing.Point(79, 11)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(127, 32)
        Me.Label47.TabIndex = 1
        Me.Label47.Text = "Play Music"
        '
        'PictureBox31
        '
        Me.PictureBox31.BackgroundImage = CType(resources.GetObject("PictureBox31.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox31.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox31.Name = "PictureBox31"
        Me.PictureBox31.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox31.TabIndex = 0
        Me.PictureBox31.TabStop = False
        '
        'Panel22
        '
        Me.Panel22.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel22.Controls.Add(Me.Button24)
        Me.Panel22.Controls.Add(Me.Button40)
        Me.Panel22.Controls.Add(Me.Label48)
        Me.Panel22.Controls.Add(Me.Label49)
        Me.Panel22.Controls.Add(Me.PictureBox32)
        Me.Panel22.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel22.Location = New System.Drawing.Point(0, 72)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(808, 72)
        Me.Panel22.TabIndex = 1
        '
        'Button24
        '
        Me.Button24.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button24.BackColor = System.Drawing.Color.YellowGreen
        Me.Button24.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button24.FlatAppearance.BorderSize = 0
        Me.Button24.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button24.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button24.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button24.ForeColor = System.Drawing.Color.White
        Me.Button24.Location = New System.Drawing.Point(629, 19)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(167, 35)
        Me.Button24.TabIndex = 29
        Me.Button24.Text = "Install"
        Me.Button24.UseVisualStyleBackColor = False
        '
        'Button40
        '
        Me.Button40.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button40.BackColor = System.Drawing.Color.DimGray
        Me.Button40.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button40.FlatAppearance.BorderSize = 0
        Me.Button40.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button40.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button40.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button40.ForeColor = System.Drawing.Color.White
        Me.Button40.Location = New System.Drawing.Point(629, 19)
        Me.Button40.Name = "Button40"
        Me.Button40.Size = New System.Drawing.Size(167, 35)
        Me.Button40.TabIndex = 28
        Me.Button40.Text = "Installed"
        Me.Button40.UseVisualStyleBackColor = False
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.DimGray
        Me.Label48.Location = New System.Drawing.Point(81, 42)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(58, 20)
        Me.Label48.TabIndex = 2
        Me.Label48.Text = "Google"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.Color.Black
        Me.Label49.Location = New System.Drawing.Point(79, 11)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(170, 32)
        Me.Label49.TabIndex = 1
        Me.Label49.Text = "Google Search"
        '
        'PictureBox32
        '
        Me.PictureBox32.BackgroundImage = CType(resources.GetObject("PictureBox32.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox32.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox32.Name = "PictureBox32"
        Me.PictureBox32.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox32.TabIndex = 0
        Me.PictureBox32.TabStop = False
        '
        'Panel23
        '
        Me.Panel23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel23.Controls.Add(Me.Button23)
        Me.Panel23.Controls.Add(Me.Label50)
        Me.Panel23.Controls.Add(Me.Label51)
        Me.Panel23.Controls.Add(Me.PictureBox33)
        Me.Panel23.Controls.Add(Me.Button42)
        Me.Panel23.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel23.Location = New System.Drawing.Point(0, 0)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(808, 72)
        Me.Panel23.TabIndex = 0
        '
        'Button23
        '
        Me.Button23.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button23.BackColor = System.Drawing.Color.YellowGreen
        Me.Button23.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button23.FlatAppearance.BorderSize = 0
        Me.Button23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OliveDrab
        Me.Button23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.GreenYellow
        Me.Button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button23.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button23.ForeColor = System.Drawing.Color.White
        Me.Button23.Location = New System.Drawing.Point(629, 19)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(167, 35)
        Me.Button23.TabIndex = 28
        Me.Button23.Text = "Install"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label50.ForeColor = System.Drawing.Color.DimGray
        Me.Label50.Location = New System.Drawing.Point(81, 42)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(65, 20)
        Me.Label50.TabIndex = 2
        Me.Label50.Text = "Kingsoft"
        '
        'Label51
        '
        Me.Label51.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label51.AutoEllipsis = True
        Me.Label51.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.ForeColor = System.Drawing.Color.Black
        Me.Label51.Location = New System.Drawing.Point(79, 11)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(478, 32)
        Me.Label51.TabIndex = 1
        Me.Label51.Text = "Kingsoft Office (Includes Writer, Spreadsheet and Presentation.)"
        '
        'PictureBox33
        '
        Me.PictureBox33.BackgroundImage = CType(resources.GetObject("PictureBox33.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox33.Location = New System.Drawing.Point(4, 4)
        Me.PictureBox33.Name = "PictureBox33"
        Me.PictureBox33.Size = New System.Drawing.Size(70, 65)
        Me.PictureBox33.TabIndex = 0
        Me.PictureBox33.TabStop = False
        '
        'Button42
        '
        Me.Button42.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button42.BackColor = System.Drawing.Color.DimGray
        Me.Button42.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button42.FlatAppearance.BorderSize = 0
        Me.Button42.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray
        Me.Button42.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray
        Me.Button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button42.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button42.ForeColor = System.Drawing.Color.White
        Me.Button42.Location = New System.Drawing.Point(629, 19)
        Me.Button42.Name = "Button42"
        Me.Button42.Size = New System.Drawing.Size(167, 35)
        Me.Button42.TabIndex = 27
        Me.Button42.Text = "Installed"
        Me.Button42.UseVisualStyleBackColor = False
        '
        'SkinnedTBar
        '
        Me.SkinnedTBar.BackColor = System.Drawing.Color.Black
        Me.SkinnedTBar.BackgroundImage = CType(resources.GetObject("SkinnedTBar.BackgroundImage"), System.Drawing.Image)
        Me.SkinnedTBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SkinnedTBar.Controls.Add(Me.PictureBox37)
        Me.SkinnedTBar.Controls.Add(Me.skinbackbtn)
        Me.SkinnedTBar.Controls.Add(Me.PictureBox39)
        Me.SkinnedTBar.Controls.Add(Me.PictureBox40)
        Me.SkinnedTBar.Controls.Add(Me.PictureBox41)
        Me.SkinnedTBar.Controls.Add(Me.PictureBox42)
        Me.SkinnedTBar.Controls.Add(Me.Label55)
        Me.SkinnedTBar.Controls.Add(Me.Label56)
        Me.SkinnedTBar.Controls.Add(Me.PictureBox43)
        Me.SkinnedTBar.Controls.Add(Me.Label57)
        Me.SkinnedTBar.Dock = System.Windows.Forms.DockStyle.Top
        Me.SkinnedTBar.Location = New System.Drawing.Point(0, 0)
        Me.SkinnedTBar.Name = "SkinnedTBar"
        Me.SkinnedTBar.Size = New System.Drawing.Size(825, 100)
        Me.SkinnedTBar.TabIndex = 38
        Me.SkinnedTBar.Visible = False
        '
        'skinbackbtn
        '
        Me.skinbackbtn.BackColor = System.Drawing.Color.Transparent
        Me.skinbackbtn.BackgroundImage = CType(resources.GetObject("skinbackbtn.BackgroundImage"), System.Drawing.Image)
        Me.skinbackbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.skinbackbtn.Location = New System.Drawing.Point(185, 18)
        Me.skinbackbtn.Name = "skinbackbtn"
        Me.skinbackbtn.Size = New System.Drawing.Size(65, 63)
        Me.skinbackbtn.TabIndex = 46
        Me.skinbackbtn.TabStop = False
        Me.skinbackbtn.Visible = False
        '
        'PictureBox39
        '
        Me.PictureBox39.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox39.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox39.BackgroundImage = CType(resources.GetObject("PictureBox39.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox39.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox39.Location = New System.Drawing.Point(758, 8)
        Me.PictureBox39.Name = "PictureBox39"
        Me.PictureBox39.Size = New System.Drawing.Size(29, 56)
        Me.PictureBox39.TabIndex = 45
        Me.PictureBox39.TabStop = False
        '
        'PictureBox40
        '
        Me.PictureBox40.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox40.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox40.BackgroundImage = CType(resources.GetObject("PictureBox40.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox40.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox40.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox40.Location = New System.Drawing.Point(725, 8)
        Me.PictureBox40.Name = "PictureBox40"
        Me.PictureBox40.Size = New System.Drawing.Size(29, 56)
        Me.PictureBox40.TabIndex = 44
        Me.PictureBox40.TabStop = False
        '
        'PictureBox41
        '
        Me.PictureBox41.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox41.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox41.BackgroundImage = CType(resources.GetObject("PictureBox41.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox41.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox41.Location = New System.Drawing.Point(790, 8)
        Me.PictureBox41.Name = "PictureBox41"
        Me.PictureBox41.Size = New System.Drawing.Size(29, 56)
        Me.PictureBox41.TabIndex = 43
        Me.PictureBox41.TabStop = False
        '
        'PictureBox42
        '
        Me.PictureBox42.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox42.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox42.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.User_9__Carré_
        Me.PictureBox42.Location = New System.Drawing.Point(633, 9)
        Me.PictureBox42.Name = "PictureBox42"
        Me.PictureBox42.Size = New System.Drawing.Size(56, 55)
        Me.PictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox42.TabIndex = 41
        Me.PictureBox42.TabStop = False
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.BackColor = System.Drawing.Color.DodgerBlue
        Me.Label55.Font = New System.Drawing.Font("Kozuka Gothic Pr6N R", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(93, 19)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(56, 26)
        Me.Label55.TabIndex = 39
        Me.Label55.Text = "FEREN"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.BackColor = System.Drawing.Color.Transparent
        Me.Label56.Font = New System.Drawing.Font("Kozuka Gothic Pro EL", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(88, 33)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(101, 48)
        Me.Label56.TabIndex = 40
        Me.Label56.Text = "store"
        '
        'PictureBox43
        '
        Me.PictureBox43.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox43.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.PictureBox43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox43.Location = New System.Drawing.Point(8, 8)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(79, 84)
        Me.PictureBox43.TabIndex = 38
        Me.PictureBox43.TabStop = False
        '
        'Label57
        '
        Me.Label57.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label57.BackColor = System.Drawing.Color.Transparent
        Me.Label57.Font = New System.Drawing.Font("Kozuka Gothic Pro L", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(-68, 9)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(702, 55)
        Me.Label57.TabIndex = 42
        Me.Label57.Text = "User1"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel26
        '
        Me.Panel26.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel26.Controls.Add(Me.Button48)
        Me.Panel26.Controls.Add(Me.Button47)
        Me.Panel26.Controls.Add(Me.Button46)
        Me.Panel26.Controls.Add(Me.Button45)
        Me.Panel26.Location = New System.Drawing.Point(376, 70)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(343, 194)
        Me.Panel26.TabIndex = 39
        Me.Panel26.Visible = False
        '
        'Button48
        '
        Me.Button48.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Skin3
        Me.Button48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button48.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.Button48.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.Button48.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke
        Me.Button48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button48.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button48.ForeColor = System.Drawing.Color.Black
        Me.Button48.Location = New System.Drawing.Point(174, 100)
        Me.Button48.Name = "Button48"
        Me.Button48.Size = New System.Drawing.Size(152, 84)
        Me.Button48.TabIndex = 3
        Me.Button48.Text = "2 by Box0ovn"
        Me.Button48.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button48.UseVisualStyleBackColor = True
        '
        'Button47
        '
        Me.Button47.BackgroundImage = CType(resources.GetObject("Button47.BackgroundImage"), System.Drawing.Image)
        Me.Button47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button47.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.Button47.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.Button47.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke
        Me.Button47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button47.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button47.ForeColor = System.Drawing.Color.Black
        Me.Button47.Location = New System.Drawing.Point(16, 100)
        Me.Button47.Name = "Button47"
        Me.Button47.Size = New System.Drawing.Size(152, 84)
        Me.Button47.TabIndex = 2
        Me.Button47.Text = "32 by Box0ovn"
        Me.Button47.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button47.UseVisualStyleBackColor = True
        '
        'Button46
        '
        Me.Button46.BackgroundImage = CType(resources.GetObject("Button46.BackgroundImage"), System.Drawing.Image)
        Me.Button46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button46.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.Button46.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.Button46.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke
        Me.Button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button46.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button46.ForeColor = System.Drawing.Color.Black
        Me.Button46.Location = New System.Drawing.Point(174, 10)
        Me.Button46.Name = "Button46"
        Me.Button46.Size = New System.Drawing.Size(152, 84)
        Me.Button46.TabIndex = 1
        Me.Button46.Text = "49 by Box0ovn"
        Me.Button46.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button46.UseVisualStyleBackColor = True
        '
        'Button45
        '
        Me.Button45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button45.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke
        Me.Button45.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro
        Me.Button45.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke
        Me.Button45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button45.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button45.ForeColor = System.Drawing.Color.Black
        Me.Button45.Location = New System.Drawing.Point(16, 10)
        Me.Button45.Name = "Button45"
        Me.Button45.Size = New System.Drawing.Size(152, 84)
        Me.Button45.TabIndex = 0
        Me.Button45.Text = "Default"
        Me.Button45.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button45.UseVisualStyleBackColor = True
        '
        'StoreDesktop
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(825, 508)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel26)
        Me.Controls.Add(Me.SkinnedTBar)
        Me.Controls.Add(Me.PictureBox36)
        Me.Controls.Add(Me.backbtn)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.Closable)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.FrontPage)
        Me.Controls.Add(Me.WebBrowsers)
        Me.Controls.Add(Me.Antiviruses)
        Me.Controls.Add(Me.everdaylife)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "StoreDesktop"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Store"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FrontPage.ResumeLayout(False)
        Me.NoConnection.ResumeLayout(False)
        Me.NoConnection.PerformLayout()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WebBrowsers.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Antiviruses.ResumeLayout(False)
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel24.ResumeLayout(False)
        Me.Panel24.PerformLayout()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.backbtn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.everdaylife.ResumeLayout(False)
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SkinnedTBar.ResumeLayout(False)
        Me.SkinnedTBar.PerformLayout()
        CType(Me.skinbackbtn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel26.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Closable As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents FrontPage As System.Windows.Forms.Panel
    Friend WithEvents WebBrowsers As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Antiviruses As System.Windows.Forms.Panel
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents PictureBox24 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents PictureBox25 As System.Windows.Forms.PictureBox
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Panel16 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents PictureBox26 As System.Windows.Forms.PictureBox
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Panel17 As System.Windows.Forms.Panel
    Friend WithEvents Button29 As System.Windows.Forms.Button
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents PictureBox27 As System.Windows.Forms.PictureBox
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Panel18 As System.Windows.Forms.Panel
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents PictureBox28 As System.Windows.Forms.PictureBox
    Friend WithEvents Button32 As System.Windows.Forms.Button
    Friend WithEvents Button33 As System.Windows.Forms.Button
    Friend WithEvents backbtn As System.Windows.Forms.PictureBox
    Friend WithEvents everdaylife As System.Windows.Forms.Panel
    Friend WithEvents Panel19 As System.Windows.Forms.Panel
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents PictureBox29 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel20 As System.Windows.Forms.Panel
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents PictureBox30 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel21 As System.Windows.Forms.Panel
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents PictureBox31 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel22 As System.Windows.Forms.Panel
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents PictureBox32 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel23 As System.Windows.Forms.Panel
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents PictureBox33 As System.Windows.Forms.PictureBox
    Friend WithEvents Button42 As System.Windows.Forms.Button
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents PictureBox34 As System.Windows.Forms.PictureBox
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button36 As System.Windows.Forms.Button
    Friend WithEvents Button35 As System.Windows.Forms.Button
    Friend WithEvents Button37 As System.Windows.Forms.Button
    Friend WithEvents Button40 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Panel24 As System.Windows.Forms.Panel
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents PictureBox35 As System.Windows.Forms.PictureBox
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button44 As System.Windows.Forms.Button
    Friend WithEvents Button43 As System.Windows.Forms.Button
    Friend WithEvents Button41 As System.Windows.Forms.Button
    Friend WithEvents Button39 As System.Windows.Forms.Button
    Friend WithEvents Button38 As System.Windows.Forms.Button
    Friend WithEvents Button34 As System.Windows.Forms.Button
    Friend WithEvents PictureBox36 As System.Windows.Forms.PictureBox
    Friend WithEvents SkinnedTBar As System.Windows.Forms.Panel
    Friend WithEvents PictureBox37 As System.Windows.Forms.PictureBox
    Friend WithEvents skinbackbtn As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox39 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox40 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox41 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox42 As System.Windows.Forms.PictureBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents PictureBox43 As System.Windows.Forms.PictureBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Panel26 As System.Windows.Forms.Panel
    Friend WithEvents Button45 As System.Windows.Forms.Button
    Friend WithEvents Button47 As System.Windows.Forms.Button
    Friend WithEvents Button46 As System.Windows.Forms.Button
    Friend WithEvents Button48 As System.Windows.Forms.Button
    Friend WithEvents NoConnection As System.Windows.Forms.Panel
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents PictureBox38 As System.Windows.Forms.PictureBox

End Class
